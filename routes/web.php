<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
   $blog = \App\Course::first();
   dd($blog);
});

Route::get('/', 'FrontEndController@front')->name('front');
Route::get('/about', 'FrontEndController@about')->name('about');
Route::get('/service', 'FrontEndController@service')->name('service');
Route::get('/message', 'FrontEndController@message')->name('message');
Route::get('/testimonials', 'FrontEndController@testimonial')->name('testimonial');
Route::get('/faq', 'FrontEndController@faq')->name('faq');
Route::get('/course/{title}', 'FrontEndController@course')->name('course');
Route::get('/gallery', 'FrontEndController@gallery')->name('gallery');
Route::get('/contact', 'FrontEndController@contact')->name('contact');
Route::get('/blog', 'FrontEndController@blog')->name('blog');
Route::get('/blog/{id}/detail', 'FrontEndController@blogsingle')->name('blogsingle');
Route::get('/blog/search', 'FrontEndController@search')->name('search');


Route::get('/privacy_policy', 'FrontEndController@privacy')->name('privacy');
Route::get('/terms', 'FrontEndController@terms')->name('terms');

Route::get('/preparation-class/{slug}', 'FrontEndController@class')->name('class');
Route::get('/apply', 'FrontEndController@apply')->name('apply');


Auth::routes();


Route::match(['get', 'post'],'admin/login' ,'AdminController@login')->name('admin.login');

//Password reset routes
Route::post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin/password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');


Route::group(['middleware' => 'auth'], function(){
    // Admin Settings and Login
    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/admin/profile/{id}', 'AdminController@profile')->name('profile');
    Route::post('/admin/update/profile/{id}', 'AdminController@updateProfile')->name('update.profile');
    Route::match(['get', 'post'], '/admin/edit/password', 'AdminController@editPassword')->name('edit.password');
    Route::post('/admin/edit/check-password', 'AdminController@chkUserPassword');

    // Site Settings
    Route::get('/admin/site-settings', 'SiteSettingController@site')->name('site.settings');
    Route::post('/admin/site-settings/update/{id}', 'SiteSettingController@update')->name('site.update');

    // About Us
    Route::get('/admin/pages/about', 'PagesController@about')->name('page.about');
    Route::post('/admin/pages/about/update/{id}', 'PagesController@updateabout')->name('about.update');

   // Prospectus
    Route::get('/admin/pages/prospectus', 'PagesController@prospectus')->name('page.prospectus');
    Route::post('/admin/pages/prospectus/update/{id}', 'PagesController@prospectusupdate')->name('prospectus.update');

    // Message From MD
    Route::get('/admin/pages/message', 'MessageController@message')->name('page.message');
    Route::post('/admin/pages/message/{id}/update', 'MessageController@updatemessage')->name('messagepage.update');

    // Service page
    Route::get('/admin/pages/service', 'PagesController@service')->name('page.service');
    Route::post('/admin/pages/service/update/{id}', 'PagesController@updateservice')->name('servicepage.update');

    // Faq Pages
    Route::get('/admin/pages/faq', 'FaqPageController@faqpage')->name('faqpage');
    Route::post('/admin/pages/faq/update/{id}', 'FaqPageController@updatefaq')->name('faqpage.update');

    // Testimonial Page
    Route::get('/admin/pages/testimonial', 'TestimonialController@testimonialpage')->name('testimonialpage');
    Route::post('/admin/pages/testimonial/update/{id}', 'TestimonialController@updatetestimonial')->name('testimonialpage.update');

    // Gallery Page
    Route::get('/admin/pages/gallery', 'GalleryController@gallerypage')->name('gallerypage');
    Route::post('/admin/pages/gallery/update/{id}', 'GalleryController@updategallery')->name('gallerypage.update');

    // Contact Us page
    Route::get('/admin/pages/contact-us', 'ContactUsController@contactypage')->name('contactpage');
    Route::post('/admin/pages/contact-us/update/{id}', 'ContactUsController@updatecontact')->name('contactpage.update');

    // Blog Page
    Route::get('/admin/pages/blog', 'BlogPageController@blogpage')->name('blogpage');
    Route::post('/admin/pages/blog/update/{id}', 'BlogPageController@updateblogpage')->name('blogpage.update');

    // Branch
    Route::get('/admin/branch/create', 'BranchController@create')->name('branch.create');
    Route::post('/admin/branch/store', 'BranchController@store')->name('branch.store');
    Route::get('/admin/branch/', 'BranchController@index')->name('branch.index');
    Route::get('/admin/branch/edit/{id}', 'BranchController@edit')->name('branch.edit');
    Route::post('/admin/branch/update/{id}', 'BranchController@update')->name('branch.update');
    Route::get('/admin/delete-branch/{id}', 'BranchController@delete')->name('branch.delete');

    // Team
    Route::get('/admin/team/create', 'TeamController@create')->name('team.create');
    Route::post('/admin/team/store', 'TeamController@store')->name('team.store');
    Route::get('/admin/team/', 'TeamController@index')->name('team.index');
    Route::get('/admin/team/edit/{id}', 'TeamController@edit')->name('team.edit');
    Route::post('/admin/team/update/{id}', 'TeamController@update')->name('team.update');
    Route::get('/admin/delete-team/{id}', 'TeamController@delete')->name('team.delete');

    // Partner
    Route::get('/admin/partner/create', 'PartnerController@create')->name('partner.create');
    Route::post('/admin/partner/store', 'PartnerController@store')->name('partner.store');
    Route::get('/admin/partner/', 'PartnerController@index')->name('partner.index');
    Route::get('/admin/partner/edit/{id}', 'PartnerController@edit')->name('partner.edit');
    Route::post('/admin/partner/update/{id}', 'PartnerController@update')->name('partner.update');
    Route::get('/admin/delete-partner/{id}', 'PartnerController@delete')->name('partner.delete');

    // Slider
    Route::get('/admin/slider/create', 'SliderController@create')->name('slider.create');
    Route::post('/admin/slider/store', 'SliderController@store')->name('slider.store');
    Route::get('/admin/slider/', 'SliderController@index')->name('slider.index');
    Route::get('/admin/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
    Route::post('/admin/slider/update/{id}', 'SliderController@update')->name('slider.update');
    Route::get('/admin/delete-slider/{id}', 'SliderController@delete')->name('slider.delete');

    // Faq
    Route::get('/admin/faq/create', 'FaqController@create')->name('faq.create');
    Route::post('/admin/faq/store', 'FaqController@store')->name('faq.store');
    Route::get('/admin/faq/', 'FaqController@index')->name('faq.index');
    Route::get('/admin/faq/edit/{id}', 'FaqController@edit')->name('faq.edit');
    Route::post('/admin/faq/update/{id}', 'FaqController@update')->name('faq.update');
    Route::get('/admin/delete-faq/{id}', 'FaqController@delete')->name('faq.delete');

    // Testimonial
    Route::get('/admin/testimonial/create', 'TestimonialController@create')->name('testimonial.create');
    Route::post('/admin/testimonial/store', 'TestimonialController@store')->name('testimonial.store');
    Route::get('/admin/testimonial/', 'TestimonialController@index')->name('testimonial.index');
    Route::get('/admin/testimonial/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
    Route::post('/admin/testimonial/update/{id}', 'TestimonialController@update')->name('testimonial.update');
    Route::get('/admin/delete-testimonial/{id}', 'TestimonialController@delete')->name('testimonial.delete');

    // Course
    Route::get('/admin/course/create', 'CourseController@create')->name('course.create');
    Route::post('/admin/course/store', 'CourseController@store')->name('course.store');
    Route::get('/admin/course/', 'CourseController@index')->name('course.index');
    Route::get('/admin/course/edit/{id}', 'CourseController@edit')->name('course.edit');
    Route::post('/admin/course/update/{id}', 'CourseController@update')->name('course.update');
    Route::get('/admin/delete-course/{id}', 'CourseController@destroy')->name('course.delete');
    Route::post('/admin/delete-brochure/{id}', 'CourseController@brochuredestroy')->name('brochure.delete');

    // Gallery
    Route::get('/admin/gallery/create', 'GalleryController@create')->name('gallery.create');
    Route::post('/admin/gallery/store', 'GalleryController@store')->name('gallery.store');
    Route::get('/admin/gallery/', 'GalleryController@index')->name('gallery.index');
    Route::get('/admin/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
    Route::post('/admin/gallery/update/{id}', 'GalleryController@update')->name('gallery.update');
    Route::get('/admin/delete-gallery/{id}', 'GalleryController@destroy')->name('gallery.delete');

    // Contact Us
    Route::get('/admin/contact/create', 'ContactUsController@create')->name('contact.create');
    Route::get('/admin/contact/', 'ContactUsController@index')->name('contact.index');
    Route::get('/admin/contact/edit/{id}', 'ContactUsController@edit')->name('contact.edit');
    Route::post('/admin/contact/update/{id}', 'ContactUsController@update')->name('contact.update');
    Route::get('/admin/delete-contact/{id}', 'ContactUsController@destroy')->name('contact.delete');
    Route::get('/admin/inbox/seen/{id}', 'ContactUsController@seen')->name('contact.seen');

    // Blog Category
    Route::get('/admin/category/create', 'BlogCategoryController@create')->name('category.create');
    Route::post('/admin/category/store', 'BlogCategoryController@store')->name('category.store');
    Route::get('/admin/category/', 'BlogCategoryController@index')->name('category.index');
    Route::get('/admin/category/edit/{id}', 'BlogCategoryController@edit')->name('category.edit');
    Route::post('/admin/category/update/{id}', 'BlogCategoryController@update')->name('category.update');
    Route::get('/admin/delete-category/{id}', 'BlogCategoryController@destroy')->name('category.delete');
    Route::get('/admin/category/publish/{id}', 'BlogCategoryController@publish')->name('category.publish');


    // Blog Post
    Route::get('/admin/blog/create', 'BlogPostController@create')->name('blog.create');
    Route::post('/admin/blog/store', 'BlogPostController@store')->name('blog.store');
    Route::get('/admin/blog/', 'BlogPostController@index')->name('blog.index');
    Route::get('/admin/blog/edit/{id}', 'BlogPostController@edit')->name('blog.edit');
    Route::post('/admin/blog/update/{id}', 'BlogPostController@update')->name('blog.update');
    Route::get('/admin/delete-blog/{id}', 'BlogPostController@destroy')->name('blog.delete');
    Route::get('/admin/blog/publish/{id}', 'BlogPostController@publish')->name('blog.publish');

    // Preparation Class
    Route::get('/admin/preparation/create', 'PreparationClassController@create')->name('preparation.create');
    Route::post('/admin/preparation/store', 'PreparationClassController@store')->name('preparation.store');
    Route::get('/admin/preparation/', 'PreparationClassController@index')->name('preparation.index');
    Route::get('/admin/preparation/edit/{id}', 'PreparationClassController@edit')->name('preparation.edit');
    Route::post('/admin/preparation/update/{id}', 'PreparationClassController@update')->name('preparation.update');
    Route::get('/admin/delete-preparation/{id}', 'PreparationClassController@destroy')->name('preparation.delete');

    // Blog Tag
    Route::get('/admin/tag/create', 'TagController@create')->name('tag.create');
    Route::post('/admin/tag/store', 'TagController@store')->name('tag.store');
    Route::get('/admin/tag/', 'TagController@index')->name('tag.index');
    Route::get('/admin/tag/edit/{id}', 'TagController@edit')->name('tag.edit');
    Route::post('/admin/tag/update/{id}', 'TagController@update')->name('tag.update');
    Route::get('/admin/delete-tag/{id}', 'TagController@destroy')->name('tag.delete');

    // Enquire
    Route::get('/admin/enquire/create', 'EnquireController@create')->name('enquire.create');
    Route::get('/admin/enquire/', 'EnquireController@index')->name('enquire.index');
    Route::get('/admin/enquire/edit/{id}', 'EnquireController@edit')->name('enquire.edit');
    Route::post('/admin/enquire/update/{id}', 'EnquireController@update')->name('enquire.update');
    Route::get('/admin/delete-enquire/{id}', 'EnquireController@destroy')->name('enquire.delete');
    Route::get('/admin/enquire/seen/{id}', 'EnquireController@seen')->name('enquire.seen');

    // Candidate
    Route::get('/admin/candidate/create', 'OnlineApplyController@create')->name('candidate.create');
    Route::get('/admin/candidate/', 'OnlineApplyController@index')->name('candidate.index');
    Route::get('/admin/candidate/edit/{id}', 'OnlineApplyController@edit')->name('candidate.edit');
    Route::post('/admin/candidate/update/{id}', 'OnlineApplyController@update')->name('candidate.update');
    Route::get('/admin/delete-candidate/{id}', 'OnlineApplyController@destroy')->name('candidate.delete');
    Route::get('/admin/candidate/seen/{id}', 'OnlineApplyController@seen')->name('candidate.seen');

});

// Contact Us store
Route::post('admin/contact/store', 'ContactUsController@store')->name('contact.store');

//Enquire store
Route::post('admin/enquire/store', 'EnquireController@store')->name('enquire.store');

//online apply
Route::post('admin/candidate/store', 'OnlineApplyController@store')->name('candidate.store');

//Blog comment and reply
Route::post('/blog/comment/{id}', 'CommentController@store')->name('comment.store');
Route::post('/blog/reply/{id}', 'ReplyController@store')->name('reply.store');


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'AdminController@logout')->name('admin.logout');
