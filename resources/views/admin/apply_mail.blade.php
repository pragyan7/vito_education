<h1>Detail</h1>
<b>Name: </b> {{ $name }} <br>
<b>Email: </b> <a href="mailto:{{ $email }}"></a>{{ $email }}
<b>Phone: </b> {{ $phone }} <br>
<b>Gender: </b> {{ $gender }} <br>
<b>Address: </b> {{ $address }} <br>
<b>Country: </b> {{ $country }} <br>

<b>Course: </b> {{ $course }} <br>
<b>Preferred Year: </b> {{ $prefered_year }} <br>
<b>Qualification</b> {{ $qualification }}