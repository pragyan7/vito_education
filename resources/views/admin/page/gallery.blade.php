@extends('admin.layouts.admin_design')

@section('title')
    <title>Gallery Page Settings - Vito Education</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Gallery Page  Settings
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="javascript:">Admin</a></li>
                <li class="breadcrumb-item active">Gallery</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <h6 class="box-subtitle text-white">Gallery</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{route('gallerypage.update', $gallery->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <h5>Cover Image</h5>
                                    <div class="controls">
                                        <input type="file" name="banner" class="form-control"> </div>
                                    <br>
                                    <input type="hidden" name="current_image" value="{{$gallery->banner}}">
                                    <img src="{{asset('public/adminpanel/uploads/gallery/'.$gallery->banner)}}" alt="{{$gallery->title}}" width="500px">
                                </div>

                                <div class="form-group">
                                    <h5>Title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" id="title" class="form-control" data-validation-required-message="This field is required" value="{{$gallery->title}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Sub Heading<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="sub_title" id="subtitle" class="form-control" data-validation-required-message="This field is required" value="{{$gallery->sub_title}}"> </div>
                                </div>

                                <h5>Seo Settings</h5>

                                <div class="form-group">
                                    <h5>Seo Title</h5>
                                    <div class="controls">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="{{$gallery->seo_title}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Seo Slug</h5>
                                    <div class="controls">
                                        <input type="text" name="seo_slug" id="seo_slug" class="form-control" value="{{$gallery->seo_slug}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Focus Keyphrase</h5>
                                    <div class="controls">
                                        <input type="text" name="focus_keyphrase" id="focus_keyphrase" class="form-control" value="{{$gallery->focus_keyphrase}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Meta Description</h5>
                                    <div class="controls">
                                        <textarea name="meta_description" id="summernote" cols="30" rows="10" class="form-control my-editor summernote">
                                            {{$gallery->meta_description}}
                                        </textarea>
                                    </div>




                                </div>
                                <br>

                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Please Type Here',
            tabsize: 2,
            height: 100
        });
    </script>
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        ! function(window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
