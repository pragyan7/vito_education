@extends('admin.layouts.admin_design')

@section('title')
    <title>Candidates - Vito Education</title>
@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Inbox
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="javascript:">Online Applied</a></li>
                <li class="breadcrumb-item active">View Candidates</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">



                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">Candidates</h4>

                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>address</th>
                                        <th>Mail</th>
                                        <th>Phone number</th>
                                        <th>Qualification</th>
                                        <th>Course</th>
                                        <th>Preffered Year</th>
                                        <th>Status</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($candidates as $candidate)
                                        <tr>
                                            <td>{{$loop->index +1}}</td>
                                            <td>{{$candidate->full_name}}</td>
                                            <td>{{$candidate->gender}}</td>
                                            <td>{{$candidate->address.', '.$candidate->country}}</td>
                                            <td><a href="mailto:{{ $candidate->email }}">{{$candidate->email}}</a></td>
                                            <td>{{$candidate->prefix.' '.$candidate->phone}}</td>
                                            <td>{{$candidate->qualification}}</td>
                                            <td>{{$candidate->Course->title}}</td>
                                            <td>{{$candidate->prefered_year}}</td>
                                            <td>{!! $candidate->seen() !!}</td>
                                            <td>
                                                <a href="javascript:" rel="{{$candidate->id}}" rel1="delete-candidate" class="btn btn-danger btn-xs deleteRecord" data-toggle="tooltip" title="Delete" data-original-title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script src="{{asset('public/adminpanel/assets/vendor_components/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('public/adminpanel/js/pages/data-table.js')}}"></script>

    <script>
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            // alert(deleteFunction);
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function(){
                    window.location.href="/admin/"+deleteFunction+"/"+id;
                });

        });
    </script>
@endsection
