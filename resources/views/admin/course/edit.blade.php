@extends('admin.layouts.admin_design')

@section('title')
    <title>Edit Course - Vito Education</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Course
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:">Admin</a></li>
                <li class="breadcrumb-item active">Course</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <h6 class="box-subtitle text-white">Courses</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{route('course.update', $course->id)}}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <h5>Cover Image</h5>
                                    <div class="controls">
                                        <input type="file" name="banner" class="form-control"></div>
                                    <br>
                                    <input type="hidden" name="current_image" value="{{$course->banner}}">
                                    <img src="{{asset('public/adminpanel/uploads/pages/courses/'.$course->banner)}}"
                                         alt="{{$course->title}}" width="500px">
                                </div>

                                <div class="form-group">
                                    <h5>Title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" id="title" class="form-control"
                                               data-validation-required-message="This field is required"
                                               value="{{ $course->title }}"></div>
                                </div>

                                <div class="form-group">
                                    <h5>Sub Heading<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="sub_title" id="subtitle" class="form-control"
                                               data-validation-required-message="This field is required"
                                               value="{{ $course->sub_title }}"></div>
                                </div>

                                

                                <div class="form-group">
                                    <h5>Image</h5>
                                    <div class="controls">
                                        <input type="file" name="image" class="form-control"></div>
                                    <br>
                                    <input type="hidden" name="current_image" value="{{$course->image}}">
                                    <img src="{{asset('public/adminpanel/uploads/pages/courses/'.$course->image)}}"
                                         alt="{{$course->title}}" width="500px">
                                </div>

                                <div class="form-group">
                                    <h5>Course Description <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea name="description" id="summernote" class="form-control summernote"
                                                  required placeholder="">
                                            {!! $course->description !!}
                                        </textarea>
                                    </div>
                                </div>

                                <!--<div class="form-group">
                                    <h5>Duration <span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="duration" id="duration" class="form-control"
                                               data-validation-required-message="This field is required"
                                               value="{{ $course->duration }}"></div>
                                </div>

                                <div class="form-group">
                                    <h5>Eligibility<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="eligibility" id="eligibility" class="form-control"
                                               data-validation-required-message="This field is required"
                                               value="{{ $course->eligibility }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Course Fee<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="course_fee" id="course_fee" class="form-control"
                                               data-validation-required-message="This field is required"
                                               value="{{ $course->course_fee }}"></div>
                                </div>


                                <hr>
                                <div class="form-group">
                                    <h5>Brochure</h5>
                                    <div class="controls">
                                        <input type="file" name="brochure" class="form-control"></div>
                                    <br>
                                    @if($course->brochure)
                                        <a href="{{ asset('public/adminpanel/uploads/pages/brochure/'.$course->brochure) }}" target="_blank">Brochure</a>
                                        
                                        <button type="button" class="btn btn-sm btn-danger b_button">Delete</button>
                                    @endif
                                </div>
                                <hr>-->


                                <h5>Seo Settings</h5>

                                <div class="form-group">
                                    <h5>Seo Title</h5>
                                    <div class="controls">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="{{$course->seo_title}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Seo Slug</h5>
                                    <div class="controls">
                                        <input type="text" name="seo_slug" id="seo_slug" class="form-control" value="{{$course->seo_slug}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Focus Keyphrase</h5>
                                    <div class="controls">
                                        <input type="text" name="focus_keyphrase" id="focus_keyphrase" class="form-control" value="{{$course->focus_keyphrase}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Meta Description</h5>
                                    <div class="controls">
                                        <textarea name="meta_description" id="summernote" cols="30" rows="10" class="form-control my-editor summernote">
                                            {{$course->meta_description}}
                                        </textarea>
                                    </div>




                                </div>
                                <br>

                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Edit</button>
                                </div>
                            </form>
                            <form id="b_delete" method="post" action="{{route('brochure.delete', [$course->id])}}">
                                           @csrf
                                            
                                        </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Please Type Here',
            tabsize: 2,
            height: 100
        });
    </script>
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        !function (window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
        $('#certification_list').selectize({
            plugins: ['remove_button', 'drag_drop', 'restore_on_backspace'],
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
    </script>
    <script type="text/javascript">
        $('.b_button').on('click', function(){
            $('#b_delete').submit();
        });
    </script>
@endsection
