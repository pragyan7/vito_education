@extends('admin.layouts.admin_design')

@section('title')
    <title>Edit Branch - Vito Education</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Branch
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:">Admin</a></li>
                <li class="breadcrumb-item active">Branch</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <h6 class="box-subtitle text-white">Branch</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{route('branch.update', [$branch->id])}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <h5>Branch<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="branch" id="title" class="form-control"
                                               data-validation-required-message="This field is required" value="{{$branch->branch}}"></div>
                                </div>

                                <div class="form-group">
                                    <h5>Location<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="address" id="address" class="form-control"
                                               data-validation-required-message="This field is required" value="{{$branch->address}}"></div>
                                </div>

                                <div class="form-group">
                                    <h5>Email<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="email" name="email" id="email" class="form-control"
                                               data-validation-required-message="This field is required" value="{{$branch->email}}"></div>
                                </div>

                                <div class="form-group">
                                    <h5>Phone<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="phone" id="phone" class="form-control"
                                               data-validation-required-message="This field is required" value="{{$branch->phone}}"></div>
                                </div>


                                <br>

                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Store</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Please Type Here',
            tabsize: 2,
            height: 100
        });
    </script>
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        !function (window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script type="text/javascript">
        $('#certification_list').selectize({
            plugins: ['remove_button', 'drag_drop', 'restore_on_backspace'],
            delimiter: ',',
            persist: false,
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
    </script>
@endsection
