@extends('admin.layouts.admin_design')

@section('title')
    <title>Add New Blog - Vito Education</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Add New Blog
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:">Admin</a></li>
                <li class="breadcrumb-item active">Blog</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <h6 class="box-subtitle text-white">Blogs</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{route('blog.store')}}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group">
                                    <h5>Category<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <select name="category_id" id="category_id" class="form-control"
                                                data-validation-required-message="This field is required">
                                            <option value="">-----------SELECT ONE------------</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <br>

                                    <div class="form-group">
                                        <h5>Title<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="title" id="title" class="form-control"
                                                   data-validation-required-message="This field is required"></div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Sub Heading<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="sub_title" id="subtitle" class="form-control"
                                                   data-validation-required-message="This field is required"></div>
                                    </div>


                                    <div class="form-group">
                                        <h5>Image</h5>
                                        <div class="controls">
                                            <input type="file" name="image" class="form-control"
                                                   data-validation-required-message="This field is required"></div>
                                        <br>
                                    </div>

                                    <div class="form-group">
                                        <h5>Summary </h5>
                                        <div class="controls">
                                        <textarea name="summary" id="summernote" class="form-control summernote"
                                                  placeholder="">

                                        </textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Content <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                        <textarea name="content" id="summernote" class="form-control summernote"
                                                  required placeholder="">

                                        </textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Tags</h5>

                                        <select multiple name="tags[]"
                                                id="tags">
                                            <option class="emptyTagValue" value="" selected>none</option>
                                            @foreach($tags as $tag)
                                                <option value="{{ $tag->id }}">{{ $tag->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <h5>Seo Settings</h5>

                                    <div class="form-group">
                                        <h5>Seo Title</h5>
                                        <div class="controls">
                                            <input type="text" name="seo_title" id="seo_title" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Seo Slug</h5>
                                        <div class="controls">
                                            <input type="text" name="seo_slug" id="seo_slug" class="form-control"></div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Focus Keyphrase</h5>
                                        <div class="controls">
                                            <input type="text" name="focus_keyphrase" id="focus_keyphrase"
                                                   class="form-control"></div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Meta Description</h5>
                                        <div class="controls">
                                        <textarea name="meta_description" id="summernote" cols="30" rows="10"
                                                  class="form-control my-editor summernote">
                                        </textarea>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="text-xs-right">
                                        <button type="submit" class="btn btn-info">Store</button>
                                    </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Please Type Here',
            tabsize: 2,
            height: 100
        });
    </script>
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        !function (window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script>
        $('#tags').selectize({
            plugins: ['remove_button', 'restore_on_backspace'],
            delimiter: ',',
            persist: false,
//            create: function (input) {
//                return {
//                    value: input,
//                    text: input
//                }
//            }
        });</script>
@endsection
