<footer class="main-footer">
    &copy; <?php echo date('Y'); ?> <a href="https://nextaussie.com/" target="_blank">Next Aussie Tech</a>. All Rights Reserved.
</footer>
