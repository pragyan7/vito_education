<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Pages</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('page.about')}}"><i class="iconsmind-Firefox"></i>About Us</a></li>
                    <li><a href="{{route('page.message')}}"><i class="iconsmind-Firefox"></i>Message from MD</a></li>
                    <li><a href="{{route('page.service')}}"><i class="iconsmind-Firefox"></i>Services</a></li>
                    <li><a href="{{route('faqpage')}}"><i class="iconsmind-Firefox"></i>Faq</a></li>
                    <li><a href="{{route('testimonialpage')}}"><i class="iconsmind-Firefox"></i>Testimonial</a></li>
                    <li><a href="{{route('gallerypage')}}"><i class="iconsmind-Firefox"></i>Gallery</a></li>
                    <li><a href="{{route('contactpage')}}"><i class="iconsmind-Firefox"></i>Contact Us</a></li>
                    <li><a href="{{route('blogpage')}}"><i class="iconsmind-Firefox"></i>Blog</a></li>
                    <li><a href="{{route('page.prospectus')}}"><i class="iconsmind-Firefox"></i>Prospectus</a></li>

                </ul>


            </li>


            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('site.settings')}}"><i class="iconsmind-Firefox"></i>Site Settings</a></li>

                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Branch</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('branch.create')}}"><i class="iconsmind-Firefox"></i>Add Branch</a></li>
                    <li><a href="{{route('branch.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Team</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('team.create')}}"><i class="iconsmind-Firefox"></i>Add Team</a></li>
                    <li><a href="{{route('team.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Partners</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('partner.create')}}"><i class="iconsmind-Firefox"></i>Add Partners</a></li>
                    <li><a href="{{route('partner.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Slider</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('slider.create')}}"><i class="iconsmind-Firefox"></i>Add Slider</a></li>
                    <li><a href="{{route('slider.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Faq</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('faq.create')}}"><i class="iconsmind-Firefox"></i>Add Faq</a></li>
                    <li><a href="{{route('faq.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Testimonial</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('testimonial.create')}}"><i class="iconsmind-Firefox"></i>Add Testimonial</a></li>
                    <li><a href="{{route('testimonial.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Courses</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('course.create')}}"><i class="iconsmind-Firefox"></i>Add Course</a></li>
                    <li><a href="{{route('course.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Gallery</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('gallery.create')}}"><i class="iconsmind-Firefox"></i>Add Gallery</a></li>
                    <li><a href="{{route('gallery.index')}}"><i class="iconsmind-Firefox"></i>View All</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Blog</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('category.index')}}"><i class="iconsmind-Firefox"></i>Category</a></li>
                    <li><a href="{{route('blog.index')}}"><i class="iconsmind-Firefox"></i>Blog</a></li>
                    <li><a href="{{route('tag.index')}}"><i class="iconsmind-Firefox"></i>Tag</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="iconsmind-Settings-Window"></i>
                    <span>Preparation Class</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('preparation.create')}}"><i class="iconsmind-Firefox"></i>Create</a></li>
                    <li><a href="{{route('preparation.index')}}"><i class="iconsmind-Firefox"></i>View</a></li>
                </ul>
            </li>
            <li><a href="{{route('enquire.index')}}"><i class="iconsmind-Firefox"></i>Enquire</a></li>
            <li><a href="{{route('candidate.index')}}"><i class="iconsmind-Firefox"></i>Candidate</a></li>







        </ul>


    </section>
</aside>
