@extends('admin.layouts.admin_design')

@section('title')
    <title>View Team - Vito Education</title>
@endsection

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Teams
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="javascript:">Teams</a></li>
                <li class="breadcrumb-item active">View Teams</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">



                    <div class="box box-solid box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title">View All Teams</h4>

                        </div>

                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="float-right">
                                <a href="{{route('team.create')}}" style="color: white;" class="btn btn-cyan">
                                    Add Team
                                </a>
                            </div>
                            <div class="table-responsive">
                                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Position</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($teams as $team)
                                        <tr>
                                            <td>{{$loop->index +1}}</td>
                                            <td>{{$team->name}}</td>
                                            <td>
                                                <img src="{{asset('public/adminpanel/uploads/team/'.$team->image)}}" alt="{{$team->name}}">
                                            </td>
                                            <td>{{$team->position}}</td>
                                            <td>
                                                {!! $team->description !!}
                                            </td>
                                            <td>
                                                <a href="{{route('team.edit', $team->id)}}" class="btn btn-info btn-xs " data-toggle="tooltip" title="Edit" data-original-title="Edit">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a href="javascript:" rel="{{$team->id}}" rel1="delete-team" class="btn btn-danger btn-xs deleteRecord" data-toggle="tooltip" title="Delete" data-original-title="Delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
    <script src="{{asset('public/adminpanel/assets/vendor_components/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('public/adminpanel/js/pages/data-table.js')}}"></script>

    <script>
        $(".deleteRecord").click(function(){
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            // alert(deleteFunction);
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function(){
                    window.location.href="/admin/"+deleteFunction+"/"+id;
                });

        });
    </script>
@endsection