@extends('admin.layouts.admin_design')

@section('title')
    <title>Edit Team - Vito Education</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Team Member
            </h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}"><i class="iconsmind-Library"></i></a></li>
                <li class="breadcrumb-item"><a href="javascript:">Admin</a></li>
                <li class="breadcrumb-item active"> Edit Team</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Basic Forms -->
            <div class="box box-solid box-info">
                <div class="box-header with-border">
                    <h6 class="box-subtitle text-white">Team Details</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col">
                            <form action="{{route('team.update', $team->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <h5>Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" class="form-control" data-validation-required-message="This field is required" value="{{$team->name}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Position<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="position" id="position" class="form-control" data-validation-required-message="This field is required" value="{{$team->position}}"> </div>
                                </div>

                                <div class="form-group">
                                    <h5>Image</h5>
                                    <div class="controls">
                                        <input type="file" name="image" class="form-control"> </div>
                                    <br>
                                    <input type="hidden" name="current_image" value="{{$team->image}}">
                                    <img src="{{asset('public/adminpanel/uploads/team/'.$team->image)}}" alt="{{$team->name}}" width="100px">
                                </div>

                                <div class="form-group">
                                    <h5>Description</h5>
                                    <div class="controls">
                                        <textarea name="description" id="summernote" cols="30" rows="10" class="form-control my-editor summernote">
                                            {{$team->description}}
                                        </textarea>
                                    </div>

                                    <br>
                                    <div class="form-group">
                                        <h5>Facebook Profile</h5>
                                        <div class="controls">
                                            <input type="text" name="facebook" id="facebook" class="form-control" value="{{$team->facebook}}"> </div>
                                    </div>


                                    <div class="form-group">
                                        <h5>Linkedin Profile</h5>
                                        <div class="controls">
                                            <input type="text" name="linkedin" id="linkedin" class="form-control" value="{{$team->linkedin}}"> </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Skype</h5>
                                        <div class="controls">
                                            <input type="text" name="skype" id="skype" class="form-control" value="{{$team->skype}}"> </div>
                                    </div>



                                </div>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Update</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
@endsection

@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
            placeholder: 'Please Type Here',
            tabsize: 2,
            height: 100
        });
    </script>
    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        ! function(window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection
