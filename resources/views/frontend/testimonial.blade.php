@extends('frontend.layouts.design')

@section('content')
<!-- Start Banner -->
<div class="inner-banner blog" style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/adminpanel/uploads/pages/testimonial/'.$page->banner)}}) no-repeat center top / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="content">
                    <h1 style="color: #fff;">{{$page->title}}</h1>
                    <p style="color: #fff;">{{$page->sub_title}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner -->



<!-- Start Testimonial Section -->
<section class="testimonial-outer padding-lg">
    <div class="container">
        <ul class="row testimonials">

@foreach($testimonials as $test)
            <li class="col-xs-6">
                <div class="quotblock"> <img src="{{asset('public/adminpanel/uploads/testimonial/'.$test->image)}}" class="img-responsive img-circle" alt=""> <span class="icon-quote-left-icon"></span>
                    <h3>{{$test->name}}</h3>
                    <span class="desig">{{$test->designation}}</span>
                    <p>
                     {!!  htmlspecialchars_decode($test->content) !!}
                    </p>
                </div>
            </li>
@endforeach

        </ul>

    </div>
</section>
<!-- End Testimonial Section -->
@endsection


@section('scripts')
    <script src="{{asset('public/userend/assets/masonry/js/masonry.min.js')}}"></script>
@endsection
