@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
    <div class="inner-banner blog"
         style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/adminpanel/uploads/blog/'.$blog->banner)}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h1 style="color: white !important;">{{ $blog->title }}</h1>
                        <p style="color: white !important;">{{ $blog->sub_title }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start News & Events -->
    <section class="news-wrapper padding-lg">
        <div class="container">
            <ul class="row news-listing">
                @foreach($blogs as $blog)
                    <li class="col-xs-6 col-sm-4 grid-item">
                        <div class="inner"><img src="{{asset('public/adminpanel/uploads/blog/'.$blog->image)}}" class="img-responsive"
                                                alt="{{ $blog->title }}">
                            <div class="cnt-block">
                                <ul class="post-detail">
                                    <li><span class="icon-date-icon ico"></span> <span class="bold"> {{ date('m-d-y', strtotime($blog->created_at)) }}</span></li>
                                    <li><span class="icon-chat-icon ico"></span><span class="bold">{{count($blog->comments)}}</span> Comments</li>
                                </ul>
                                <h2>{{ $blog->title }} </h2>
                                <p>{!! str_limit($blog->summary, 300) !!} </p>
                                <a href="{{route('blogsingle', [$blog->id])}}" class="read-more"><span class="icon-play-icon"></span>Read
                                    More</a></div>
                        </div>
                    </li>
                @endforeach
            </ul>
            <div class="text-center">
                <ul class="pagination blue">
                    {{$blogs->links()}}
                </ul>
            </div>
        </div>
    </section>
    <!-- End News & Events -->

@endsection