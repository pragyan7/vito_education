@include('frontend.layouts.head')



<!-- Start Banner Carousel -->
<div class="banner-outer">
    <div class="banner-slider">


        @foreach($sliders as $slider)

            <div class="slide{{$slider->id}}"
                 style="background: url({{asset('public/adminpanel/uploads/slider/'.$slider->image)}}) no-repeat center top / cover;">
                <div class="container">

                    <div class="content animated fadeInLeft">
                        <h1 class="animated fadeInLeft"
                            style="color: white; font-size: 54px !important;">{{$slider->title}}</h1>
                        <p class="animated fadeInLeft" style="color: #fff;">{{$slider->subtitle}}</p>
                        <a href="{{route('contact')}}">
                            <button class="btn animated fadeInLeft"> Know More <span class="icon-more-icon"></span>
                            </button>
                        </a>
                    </div>
                </div>

            </div>
        @endforeach

    </div>
</div>
<!-- End Banner Carousel -->

<!-- Start About Section -->
<section class="about">
    <div class="container">
        <ul class="row our-links">
            <li class="col-sm-4 apply-online clearfix equal-hight">
                <div class="icon"><img src="{{asset('public/userend/images\apply-online-ico.png')}}"
                                       class="img-responsive" alt=""></div>
                <div class="detail">
                    <h3>Apply Online</h3>
                    <p>Fill in your details and we'll call you back!</p>
                    <a href="{{ route('apply') }}" class="more"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </li>
            <li class="col-sm-4 prospects clearfix equal-hight">
                <div class="icon"><img src="{{asset('public/userend/images\prospects-ico.png')}}" class="img-responsive"
                                       alt=""></div>
                <div class="detail">
                    <h3><span>Download</span>Prospectus</h3>
                    <p>Know about us more from here.</p>
                    {{--<a target="_blank" href="{{asset('public/adminpanel/uploads/prospectus/'.$prospectus->file)}}" class="more"><i class="fa fa-angle-right" aria-hidden="true"></i></a>--}}
                    <a target="_blank" href="javascript:" class="more"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
    </li>
            <li class="col-sm-4 certification clearfix equal-hight">
                <div class="icon"><img src="{{asset('public/userend/images\certification-ico.png')}}"
                                       class="img-responsive" alt=""></div>
                <div class="detail">
                    <h3>Test Prepration</h3>
                    <p>IELTS, TOEFL-iBT, SAT and many more..</p>
                    <a href="#prep-class" class="more"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
            </li>
        </ul>
    </div>
    {{--<br>--}}
    <div class="container">
        <div class="row">
            <div class="col-sm-7 col-sm-push-5 left-block">
                <!-- <span class="sm-head">Carrer Counselor - Education</span> -->
                <h2>{{ $site->name }}</h2>
                <p style="margin-top: -20px;">
                    {!!  str_limit($about->content, 500) !!}
                </p>
                <div class="know-more-wrapper"><a href="{{ route('about') }}" class="know-more">Know More <span
                                class="icon-more-icon"></span></a></div>
            </div>
            <div class="col-sm-5 col-sm-pull-7">
                <div class="video-block">
                    <div id="thumbnail_container"><img src="{{asset('public/userend/images\about-video.jpeg')}}"
                                                       id="thumbnail" class="img-responsive" alt=""></div>
                    <a href="{!! $about->youtube_link !!}" class="start-video video"><img
                                src="{{asset('public/userend/images\play-btn.png')}}" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Section -->

<!-- Start How Study Section -->
@if(count($courses)>0)

<section class="how-study padding-lg">
    <div class="container">
        <h2><span>There are many choice</span> Explore Your Choices !</h2>
        <ul class="row">
            @foreach($courses as $course)
                <li class="col-sm-4" style="margin:0 0 20px 0;">
                    <div class="overly">
                        <div class="cnt-block">
                            <h3>{{ $course->title }}</h3>
                        </div>
                        <a href="{{route('course', [str_slug($course->title)])}}" class="more"><i
                                    class="fa fa-caret-right" aria-hidden="true"></i></a></div>
                    <figure><img src="{{asset('public/adminpanel/uploads/pages/courses/'.$course->image)}}"
                                 class="img-responsive" alt="{{ $course->title }}"
                                 style="width: 360px; height:290px;"></figure>
                </li>
            @endforeach

        </ul>


    </div>
</section>
@endif
<!-- End How Study Section -->

<!-- Start Our Importance Section -->
@if(count($classes)>0)
<section id="prep-class" class="our-impotance padding-lg2">
    <div class="container">
        <h2><span>Preparation Classes</span> We prepare students for</h2>

        <ul class="row">
            @foreach($classes as $class)
                <a href="{{route('class', [$class->slug])}}">
                    <li class="col-sm-2 equal-hight">
                        <div class="inner"><img
                                    src="{{asset('public/adminpanel/uploads/pages/classes/'.$class->image)}}"
                                    alt="Malleable Study Time">
                            <h3>{{ $class->title }}</h3>

                        </div>
                    </li>
                </a>
            @endforeach
        </ul>
    </div>
</section>
@endif
<!-- End Our Importance Section -->


<!-- Start Why Choose Section -->
<section class="why-choose padding-lg">
    <div class="container">
        <h2><span>The Numbers Say it All</span>Why Choose Us?</h2>
        <ul class="our-strength">
            <li>
                <div class="icon"><span class="icon-certification-icon"> </span></div>
                <span class="counter">{{ $about->certified_courses }}</span>
                <div class="title">Certified Courses</div>
            </li>
            <li>
                <div class="icon"><span class="icon-student-icon"></span></div>
                <span class="counter">{{ $about->students_serviced }}</span>
                <div class="title">Students Serviced</div>
            </li>
            <li>
                <div class="icon"><span class="icon-book-icon"></span></div>
                <div class="couter-outer"><span class="counter">{{ $about->universities }}</span><span></span></div>
                <div class="title">Universities</div>
            </li>
            <li>
                <div class="icon"><span class="icon-parents-icon"></span></div>
                <div class="couter-outer"><span class="counter">{{ $about->students }}</span><span>%</span></div>
                <div class="title">Satisfied Students</div>
            </li>
        </ul>
    </div>
</section>
<!-- End Why Choose Section -->

<!-- Start New & Events Section -->
@if(count($blogs)>0)

<section class="news-events padding-lg">
    <div class="container">
        <h2><span>There are many ways to learn</span>News and events</h2>
        <ul class="row cs-style-3">
            @foreach($blogs as $blog)
                <li class="col-sm-4">
                    <div class="inner">
                        <figure><img src="{{asset('public/adminpanel/uploads/blog/'.$blog->image)}}"
                                     class="img-responsive">
                            <figcaption>
                                <div class="cnt-block"><a href="{{route('blogsingle', [$blog->id])}}" class="plus-icon">+</a>
                                    <h3>{{ $blog->title }}</h3>
                                    <div class="bottom-block clearfix">
                                        <div class="date">
                                            <div class="icon"><span class="icon-calander-icon"></span></div>
                                            {{ date('m-D-Y', strtotime($blog->created_at)) }}
                                        </div>
                                        <div class="comment">
                                            <div class="icon"><span class="icon-chat-icon"></span></div>
                                            <span>{{count($blog->comments)}}</span> comments
                                        </div>
                                    </div>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </li>
            @endforeach
        </ul>
        <div class="know-more-wrapper"><a href="{{ route('blog') }}" class="know-more">More Post <span
                        class="icon-more-icon"></span></a></div>
    </div>
</section>
@endif
<!-- End New & Events Section -->

<!-- Start Campus Tour Section -->
@if(count($galleries)>0)
<section class="campus-tour padding-lg">
    <div class="container">
        <h2><span>We're proud of Beautiful Premises</span>TAKE A GALLERY TOUR</h2>
    </div>
    <ul class="gallery clearfix">
        @foreach($galleries as $gallery)
            <li>
                <div class="overlay">
                    <h3>{{ $gallery->title }}</h3>
                    <p>{{ $gallery->sub_title }}</p>
                    <a class="galleryItem" href="{{asset('public/adminpanel/uploads/gallery/'.$gallery->image)}}">
                        <span class="icon-enlarge-icon"></span></a></div>
                <figure><img src="{{asset('public/adminpanel/uploads/gallery/'.$gallery->image)}}"
                             class="img-responsive" alt=""
                             style="height: 288px" ; width="288px" ;></figure>
            </li>
        @endforeach
    </ul>
</section>
@endif
<!-- End Campus Tour Section -->

<!-- Start logos Section -->
@if(count($partners)>0)
<section class="logos">
    <div class="container">
        <ul class="owl-carousel clearfix">
            @foreach($partners as $partner)
                <li><a href="#"><img src="{{asset('public/adminpanel/uploads/partner/'.$partner->image)}}"
                                     class="img-responsive" alt="{{$partner->title}}">
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</section>
@endif
<!-- End logos Section -->

<!-- Start Testimonial -->
@if(count($testimonials)>0)
<section class="testimonial padding-lg">
    <div class="container">
        <div class="wrapper">
            <h2>What our Students Say</h2>
            <ul class="testimonial-slide">
                @foreach($testimonials as $testimonial)
                    <li>
                        {!! $testimonial->content !!}
                        <br>
                        <span>{{ $testimonial->name }}, <span>{{ $testimonial->designation }}</span></span></li>
                @endforeach

            </ul>
            <div id="bx-pager">
                @foreach($testimonials as $testimonial)
                    <a data-slide-index="{{ $loop->index }}" href="">
                        <img src="{{asset('public/adminpanel/uploads/testimonial/' . $testimonial->image)}}"
                             class="img-circle" alt=""
                             style="height: 72px" ; width:"72px";></a>
                @endforeach
            </div>
        </div>
</section>
@endif
<!-- End Testimonial -->

<!-- Branch Section -->
@if(count($branches)>0)
    <section id="branches" class="our-impotance padding-lg2" style="background-color: #eee; ">
        <div class="container">
            <h2><span></span> Our Branches </h2>

            <ul class="row">
                @foreach($branches as $branch)
                    <div class="col-md-3"
                         style="box-shadow: 1px 1px 2px 1px rgba(0,0,0,0.3);
                                color: #000;
                                margin: 5px;
                                min-height: 150px;
                                padding: 5px 0;" >
                        <b>{{ $branch->branch }}</b><br>
                        {{ $branch->address }}<br>
                        <a href="mailto:{{$branch->email}}">{{ $branch->email }}</a><br>
                        {{ $branch->phone }}
                    </div>
                @endforeach
            </ul>
        </div>
    </section>
@endif
<!-- End Branch Section -->

@include('frontend.layouts.footer')
