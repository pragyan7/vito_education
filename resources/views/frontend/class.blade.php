@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
    <div class="inner-banner blog" style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/userend/images/inner-banner-bg.jpeg')}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h1>{{ $class->title }}</h1>
                        <p>{{ $class->sub_title }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start Privacy -->
    <section class="privacy-wrapper padding-lg">
        <div class="container">
            {!! $class->content !!}
        </div>
    </section>
    <!-- End Privacy -->

@endsection