@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
    <div class="inner-banner blog"
         style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/adminpanel/uploads/gallery/'.$page->banner)}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h1 style="color: white !important;">{{ $page->title }}</h1>
                        <p style="color: white !important;">{{ $page->sub_title }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->



    <section class="campus-tour padding-lg">


        <ul class="gallery clearfix isotopeContainer">
            @foreach($galleries as $gallery)
                <li class="isotopeSelector contest">
                    <div class="overlay">
                        <h3>{{ $gallery->title }}</h3>
                        <p>{{ $gallery->sub_title }}</p>
                        <a class="galleryItem" href="{{asset('public/adminpanel/uploads/gallery/'.$gallery->image)}}"><span
                                    class="icon-enlarge-icon"></span></a> <a href="#" class="more"><span
                                    class="icon-gallery-more-arrow"></span></a></div>
                    <figure><img src="{{asset('public/adminpanel/uploads/gallery/'.$gallery->image)}}" class="img-responsive" alt=""></figure>
                </li>
            @endforeach
        </ul>
    </section>
@endsection