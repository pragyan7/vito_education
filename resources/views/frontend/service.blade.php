@section('style')
    .about.inner p {
    padding-bottom: 0px;
    color: black;
    font-family: 'Roboto', sans-serif;
    }
@endsection
@include('frontend.layouts.head')

<!-- Start Banner -->
<div class="inner-banner blog"
     style="display: block; padding: 60px 0;  min-height: 290px;  background: url({{asset('public/adminpanel/uploads/pages/'.$service->banner)}}) no-repeat center top / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="content">
                    <h1 style="color: #fff;">{{$service->title}}</h1>
                    <p style="color: #fff;">{{$service->sub_title}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner -->


<!-- Start About -->
<section class="about inner padding-lg">
    <div class="container">
        <div class="row">
            <div class="col-md-7 left-block">
                {!! $service->content !!}
            </div>
            <div class="col-md-5 about-right"><img src="{{asset('public/adminpanel/uploads/pages/'.$service->image)}}"
                                                   class="img-responsive" alt=""></div>
        </div>
    </div>
</section>
<!-- End About -->


<!-- Start Why Choose -->
<section class="why-choose grey-bg padding-lg">
    <div class="container">
        <h2><span>The Numbers Say it All</span>Why Choose Us?</h2>
        <ul class="our-strength opt2">
            <li>
                <div class="icon"><span class="icon-certification-icon"> </span></div>
                <span class="counter">{{$about->certified_courses}}</span>
                <div class="title">Certified Courses</div>
            </li>
            <li>
                <div class="icon"><span class="icon-student-icon"></span></div>
                <span class="counter">{{$about->students_serviced}}</span>
                <div class="title">Students Serviced</div>
            </li>
            <li>
                <div class="icon"><span class="icon-book-icon"></span></div>
                <div class="couter-outer"><span class="counter">{{$about->universities}}</span><span></span></div>
                <div class="title">Universities</div>

            </li>
            <li>
                <div class="icon"><span class="icon-parents-icon"></span></div>
                <div class="couter-outer"><span class="counter">{{$about->students}}</span><span>%</span></div>
                <div class="title">Satisfied Students</div>
            </li>
        </ul>
    </div>
</section>
<!-- End Why Choose -->


<!-- Start Our Importance Section -->
<section class="our-impotance padding-lg2">
    <div class="container">
        <h2><span>Preparation Classes</span> We prepare students for</h2>
        <ul class="row">
            @foreach($classes as $class)
                <a href="{{route('class', [$class->slug])}}">
                    <li class="col-sm-2 equal-hight">
                        <div class="inner"><img
                                    src="{{asset('public/adminpanel/uploads/pages/classes/'.$class->image)}}"
                                    alt="Malleable Study Time">
                            <h3>{{ $class->title }}</h3>

                        </div>
                    </li>
                </a>
            @endforeach
        </ul>
    </div>
</section>
<!-- End Our Importance Section -->


<!-- Start Browse Teacher -->
<section class="browse-teacher grey-bg padding-lg">
    <div class="container">
        <h2><span>These are the best</span> OUR TEAM</h2>
        <ul class="row browse-teachers-list clearfix">
            @foreach($teams as $team)
                <li class="col-xs-6 col-sm-3">
                    <figure><img src="{{asset('public/adminpanel/uploads/team/'.$team->image)}}" width="123"
                                 height="124" alt="{{$team->name}}"></figure>
                    <h3>{{$team->name}}</h3>
                    <span class="designation">{{$team->position}}</span>
                    <p class="equal-hight"> {!! $team->description !!}</p>
                    <ul class="teachers-follow">
                        <li><a href="{{$team->facebook}}" target="_blank"><i class="fa fa-facebook"
                                                                             aria-hidden="true"></i></a></li>
                        <li><a href="{{$team->linkedin}}" target="_blank"><i class="fa fa-linkedin"
                                                                             aria-hidden="true"></i></a></li>
                        <li><a href="{{$team->skype}}" target="_blank"><i class="fa fa-skype"
                                                                          aria-hidden="true"></i></a></li>
                    </ul>
                </li>
            @endforeach
        </ul>
    </div>
</section>
<!-- end Browse Teacher -->

<!-- Start logos Section -->
<section class="logos">
    <div class="container">
        <ul class="owl-carousel clearfix">
            @foreach($partners as $partner)
                <li><a href="#"><img src="{{asset('public/adminpanel/uploads/partner/'.$partner->image)}}"
                                     class="img-responsive" alt="{{$partner->title}}"></a></li>
            @endforeach
        </ul>
    </div>
</section>
<!-- End logos Section -->

@include('frontend.layouts.footer')
