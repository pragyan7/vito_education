@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
    <div class="inner-banner blog"
         style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/userend/images/inner-banner-bg.jpeg')}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h1>Search Result</h1>
                        @if(!empty($results))
                            <p>{{ count($results) }} blog found</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start News & Events -->
    <section class="news-wrapper padding-lg">
        <div class="container">
            <ul class="row news-listing">
                @if(!empty($results))
                    @foreach($results as $result)
                        <li class="col-xs-6 col-sm-4 grid-item">
                            <div class="inner"><img src="{{asset('public/adminpanel/uploads/blog/'.$result->image)}}"
                                                    class="img-responsive"
                                                    alt="{{ $result->title }}">
                                <div class="cnt-block">
                                    <ul class="post-detail">
                                        <li><span class="icon-date-icon ico"></span> <span
                                                    class="bold"> {{ date('m-d-y', strtotime($result->created_at)) }}</span>
                                        </li>
                                        <li><span class="icon-chat-icon ico"></span><span class="bold">{{count($result->comments)}}</span>
                                            Comments
                                        </li>
                                    </ul>
                                    <h2>{{ $result->title }} </h2>
                                    <p>{!! $result->summary !!} </p>
                                    <a href="{{route('blogsingle', [$result->id])}}" class="read-more"><span
                                                class="icon-play-icon"></span>Read
                                        More</a></div>
                            </div>
                        </li>
                    @endforeach
                @else
                    <h1>No Result Found</h1>
                @endif

            </ul>
            <div class="text-center">
                 <ul class="pagination blue">
                    {{$results->links()}}
                </ul>
            </div>
        </div>
    </section>
    <!-- End News & Events -->

@endsection