<!-- Start Footer -->
<footer class="footer">
    <!-- Start Footer Top -->
    <div class="container">
        <div class="row row1">
            <div class="col-sm-3">
                <div class="footer-logo hidden-xs"><a href="{{route('front')}}"><img
                                src="{{asset('public/adminpanel/uploads/site/'.$site->footer_logo)}}"
                                class="img-responsive" alt="" style="height: 62px;"></a></div>
                <p>© <?php echo date('Y'); ?> <span>{{$site->name}}</span>. All rights reserved</p>
                <ul class="terms clearfix">
                    <li><a href="javascript:">{{$site->location}}</a></li>
                </ul>
            </div>

            <div class="col-sm-9 clearfix">

                <div class="foot-nav">
                    <h3>Quick Links</h3>
                    <ul>
                        <li><a href="{{route('front')}}">Home</a></li>
                        <li><a href="{{route('about')}}">About</a></li>
                        <li><a href="{{route('faq')}}">Faq</a></li>
                        <li><a href="{{route('testimonial')}}">Testimonial</a></li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                    </ul>
                </div>

                @php
                    $courses = App\Course::all()
                @endphp
                <div class="foot-nav">
                    <h3>Countries</h3>
                    <ul>
                        @foreach($courses as $course)
                            <li><a href="{{route('course', [str_slug($course->title)])}}">{{ $course->title }}</a></li>
                        @endforeach



                    </ul>
                </div>
                <div class="foot-nav">
                    <h3>More</h3>
                    <ul>
                        <li><a href="{{route('gallery')}}">Gallery</a></li>
                        <li><a href="{{route('privacy')}}">Privacy</a></li>
                        <li><a href="{{route('terms')}}">Terms & Conditions</a></li>
                    </ul>
                </div>

                <div class="foot-nav">
                    <h3>Connect With Us</h3>
                    <ul class="follow-us clearfix">
                        <li><a href="{{$site->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="{{$site->twitter}}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="{{$site->youtube}}"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                    </ul>

                </div>
                
                 <div class="foot-nav" style="margin-top: 10px;">
                    <h3>Targeted Location</h3>
                    <ul class="follow-us clearfix">
                        <li>Kathmandu, </li>
                        <li>Lalitpur, </li>
                        <li>Bhaktapur, </li>
                        <li>Chitwan, </li>
                        <li>Butwal, </li>
                        <li>Bhairahawa, </li>
                        <li>Biratnagar</li>
                    </ul>

                </div>

            </div>

        </div>
    </div>
    <!-- End Footer Top -->

</footer>
<!-- End Footer -->

<!-- Scroll to top -->
<a href="#" class="scroll-top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('public/userend/js/jquery.min.js')}}"></script>
<!-- Bootsrap JS -->
<script src="{{asset('public/userend/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Select2 JS -->
<script src="{{asset('public/userend/assets/select2/js/select2.min.js')}}"></script>
<!-- Match Height JS -->
<script src="{{asset('public/userend/assets/matchHeight/js/matchHeight-min.js')}}"></script>
<!-- Bxslider JS -->
<script src="{{asset('public/userend/assets/bxslider/js/bxslider.min.js')}}"></script>
<!-- Waypoints JS -->
<script src="{{asset('public/userend/assets/waypoints/js/waypoints.min.js')}}"></script>
<!-- Counter Up JS -->
<script src="{{asset('public/userend/assets/counterup/js/counterup.min.js')}}"></script>
<!-- Magnific Popup JS -->
<script src="{{asset('public/userend/assets/magnific-popup/js/magnific-popup.min.js')}}"></script>
<!-- Owl Carousal JS -->
<script src="{{asset('public/userend/assets/owl-carousel/js/owl.carousel.min.js')}}"></script>
<!-- Modernizr JS -->
<script src="{{asset('public/userend/js/modernizr.custom.js')}}"></script>

<script src="{{asset('public/adminpanel/assets/vendor_components/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('public/adminpanel/assets/vendor_components/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

<!-- Custom JS -->
<script src="{{asset('public/userend/js/custom.js')}}"></script>


@yield('scripts')

</body>
</html>
