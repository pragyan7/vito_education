@php
    $courses = App\Course::all()
@endphp
        <!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0; maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Vito Education</title>
    <!-- Reset CSS -->
    <link href="{{asset('public/userend/css/reset.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('public/userend/css/fonts.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('public/userend/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{asset('public/userend/assets/select2/css/select2.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('public/userend/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Magnific Popup -->
    <link href="{{asset('public/userend/assets/magnific-popup/css/magnific-popup.css')}}" rel="stylesheet">
    <!-- Iconmoon -->
    <link href="{{asset('public/userend/assets/iconmoon/css/iconmoon.css')}}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="{{asset('public/userend/assets/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
    <!-- Animate -->
    <link href="{{asset('public/userend/css/animate.css')}}" rel="stylesheet">
    <!-- Custom Style -->
    <link href="{{asset('public/userend/css/custom.css')}}" rel="stylesheet">

    <link href="{{asset('public/adminpanel/assets/vendor_components/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <style>
        @yield('style')
    </style>

</head>
<body>


<!-- Start Preloader -->
<div id="loading">
    <div class="element">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start Header -->
<header>

    <!-- End Header top Bar -->
    <!-- Start Header Middle -->
    <div class="container header-middle">
        <div class="row"><span class="col-xs-6 col-sm-3"><a href="{{route('front')}}"><img
                            src="{{asset('public/adminpanel/uploads/site/'.$site->header_logo)}}" class="img-responsive"
                            alt=""></a></span>
            <div class="col-xs-6 col-sm-3"></div>
            <div class="col-xs-6 col-sm-9">
                <div class="contact clearfix">
                    <ul class="hidden-xs">
                        <li><span>Email</span> <a href="mailto:info@vitoeducation.com">{{$site->email}}</a></li>
                        <li><span>Contact</span> {{$site->phone}} </li>

                    </ul>

                </div>
            </div>
        </div>
    </div>
    <!-- End Header Middle -->
    <!-- Start Navigation -->
    <nav class="navbar navbar-inverse">
        <div class="container">
            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                        class="navbar-toggle collapsed" type="button"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse" id="navbar">

                <ul class="nav navbar-nav">
                    <li><a href="{{route('front')}}">Home</a></li>
                    <li class="dropdown"><a data-toggle="dropdown" href="">About Us <i class="fa fa-angle-down"
                                                                                       aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">

                            <li><a href="{{route('about')}}">WHO ARE WE?</a></li>
                            <li><a href="{{route('message')}}">MESSAGE FROM MD</a></li>
                            <li><a href="{{route('service')}}">SERVICES</a></li>
                            {{--<li><a href="{{route('testimonial')}}">Testimonials</a></li>--}}
                            {{--<li><a href="{{route('faq')}}">FAQ</a></li>--}}

                        </ul>
                    </li>

                    <li class="dropdown"><a data-toggle="dropdown" href="#">Study Abroad <i class="fa fa-angle-down"
                                                                                            aria-hidden="true"></i></a>
                        <ul class="dropdown-menu">
                            @foreach($courses as $course)
                                <li><a href="{{route('course', [str_slug($course->title)])}}">{{ strtoupper($course->title) }}</a></li>
                            @endforeach
                                {{--<li><a href="{{route('course')}}">Study in the USA</a></li>--}}
                                {{--<li><a href="">Study in the UK</a></li>--}}
                                {{--<li><a href="">Study in the New Zealand</a></li>--}}
                                {{--<li><a href="">Study in the Australia</a></li>--}}
                                {{--<li><a href="">Study in the Canada</a></li>--}}
                        </ul>
                    </li>
                    <li><a href="{{route('gallery')}}">Gallery</a></li>

                    <li><a href="{{route('blog')}}">Blog</a></li>

                    <li><a href="{{route('contact')}}">Contact</a></li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li ><a href="{{ route('apply') }}" class="btn">apply now <span class="icon-more-icon"></span></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navigation -->
</header>
<!-- End Header -->