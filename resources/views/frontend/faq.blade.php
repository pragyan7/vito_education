@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
    <div class="inner-banner blog" style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/adminpanel/uploads/pages/faq/'.$faqpage->banner)}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h1 style="color: #fff;">{{$faqpage->title}}</h1>
                        <p style="color: #fff;">{{$faqpage->sub_title}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start FAQ -->
    <section class="faq-wrapper faq2 padding-lg">
        <div class="container">

            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

              @foreach($faqs as $faq)
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$faq->id}}" aria-expanded="true" aria-controls="collapseOne"> {{$faq->question}} </a> </h4>
                    </div>
                    <div id="collapse{{$faq->id}}" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                          <p>{!! $faq->answer !!}</p>
                        </div>
                    </div>
                </div>
              @endforeach
            </div>

        </div>
    </section>
    <!-- End FAQ -->


@endsection
