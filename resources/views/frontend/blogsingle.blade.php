@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
    <div class="inner-banner blog"
         style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/userend/images/inner-banner-bg.jpeg')}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h1>{{ $blog->title }}</h1>
                        <p>{{ $blog->sub_title }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start News & Events -->
    <div class="container blog-wrapper padding-lg">
        <div class="row">
            <!-- Start Left Column -->
            <div class="col-sm-8 blog-left">
                <ul class="blog-listing detail">
                    <li><img src="{{asset('public/adminpanel/uploads/blog/'.$blog->image)}}" class="img-responsive"
                             alt="{{ $blog->title }}">
                        <h2>{{ $blog->sub_title }}</h2>
                        <ul class="post-detail">
                            <li><span class="icon-calander-icon ico"></span><span
                                        class="bold">{{ date('m-d-y', strtotime($blog->created_at)) }}</span>
                            </li>
                            <li><span class="icon-chat-icon ico"></span><span class="bold">{{ count($comments) }}</span>
                                Comments
                            </li>
                        </ul>
                        {!! $blog->content !!}
                    </li>
                </ul>
                {{--<ul class="follow-us clearfix">--}}
                {{--<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>--}}
                {{--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>--}}
                {{--<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>--}}
                {{--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>--}}
                {{--</ul>--}}
                <div class="comments-wrapper">
                    <h2>{{ count($comments) }} Comments</h2>
                    <ul class="row comments">
                        @foreach($comments as $comment)
                            <li class="col-sm-12 clearfix">
                                <div class="com-txt">
                                    <h3>{{ $comment->full_name }}
                                        <span>{{ date('M-D-Y h:i:s', strtotime($comment->created_at)) }}</span><span>@auth {{ $comment->email }} @endauth</span>
                                    </h3>
                                    <p>{!! $comment->comment !!}</p>
                                    <div>
                                        <hr>
                                        <label>Reply</label>
                                        <form id="comment-form" method="post"
                                              action="{{ route('reply.store', [$comment->id]) }}">
                                            {{ csrf_field() }}
                                            <div class="row">

                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" placeholder="Full Name"
                                                           name="full_name" required>
                                                </div>

                                                <div class="col-md-4">
                                                    <input type="email" class="form-control" placeholder="Email"
                                                           name="email" required>
                                                </div>
                                                <div class="col-md-6">
                                                    <textarea class="form-control" name="reply"
                                                              placeholder="Write your reply"></textarea>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary btn-md" name="submit">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </li>
                            @foreach($comment->replies as $reply)
                                <li class="col-xs-offset-1 col-sm-11 clearfix">
                                    <div class="com-txt">
                                        <h3>{{ $reply->full_name }}
                                            <span>{{ date('M-D-Y h:i:s', strtotime($reply->created_at)) }}</span><span>@auth {{ $reply->email }} @endauth</span>
                                        </h3>
                                        <p>{!! $reply->reply !!}</p>
                                </li>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
                <div class="leave-comment">
                    <h4>LEAVE COMMENT</h4>
                    <form action="{{ route('comment.store',[ $blog->id ]) }}" method="POST">
                        @csrf
                        <div class="row1 clearfix">
                            <input name="full_name" placeholder="Full Name" type="text" required>
                            <input name="email" placeholder="Email Address" type="email" required>
                        </div>
                        <textarea name="comment" placeholder="Message" required></textarea>
                        <button class="btn">Post Comment <span class="icon-more-icon"></span></button>
                    </form>
                </div>
            </div>
            <!-- End Left Column -->

            <!-- Start Right Column -->
            <div class="col-sm-4">
                <div class="blog-right">
                    <div class="search-block clearfix">
                        <form action="{{route('search')}}" method="GET">
                            <input name="key" placeholder="Search" type="text" required>
                            <input name="type" type="hidden" value="search">
                            <button class="search"><span class="icon-search-icon"></span></button>
                        </form>
                    </div>
                    <div class="category">
                        <h3>Category</h3>
                        <ul>
                            @foreach($categories as $category)
                                <li><a href="{{ route('search') }}?key={{$category->slug}}&&type=category"> {{ $category->title }} <span>{{ count($category->blogs) }}</span></a>
                                </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="recent-post">
                        <h3>Recent Posts</h3>
                        <ul>
                            @foreach($blogs as $blog)
                                <li class="clearfix"><a href="{{route('blogsingle', [$blog->id])}}">
                                        <div class="img-block"><img
                                                    src="{{asset('public/adminpanel/uploads/blog/'.$blog->image)}}"
                                                    class="img-responsive" alt=""></div>
                                        <div class="detail">
                                            <h4>{{ $blog->title }}</h4>
                                            <p>
                                                <span class="icon-date-icon ico"></span>{{ date('d-M-Y', strtotime($blog->created_at)) }}
                                            </p>
                                        </div>
                                    </a></li>
                            @endforeach

                        </ul>
                    </div>
                    {{--<div class="archives">--}}
                    {{--<h3>Archives</h3>--}}
                    {{--<ul>--}}
                    {{--<li><a href="#"><span class="icon-date-icon ico"></span> 20 August 2016</a></li>--}}
                    {{--<li><a href="#"><span class="icon-date-icon ico"></span> 17 May 2016</a></li>--}}
                    {{--<li><a href="#"><span class="icon-date-icon ico"></span> 06 April 2016</a></li>--}}
                    {{--<li><a href="#"><span class="icon-date-icon ico"></span> 01 March 2016</a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    <div class="tags">
                        <h3>Tags</h3>
                        <ul class="tags-list clearfix">
                            @foreach($tags as $tag)
                                <li><a href="{{route('search')}}?key={{$tag["slug"]}}&&type=tag">{{ $tag['title'] }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Right Column -->

        </div>
    </div>
    <!-- End News & Events -->

@endsection