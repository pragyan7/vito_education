@extends('frontend.layouts.design')

@section('content')
    <!-- Start Banner -->
        <div class="inner-banner contact" style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/adminpanel/uploads/pages/contact/'.$page->banner)}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-lg-9">
                    <div class="content">
                        <h1 style="color: white !important;">{{ $page->title }}</h1>
                        <p style="color: white !important;">{{ $page->sub_title }}</p>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-3"> <a href="{{route('apply')}}" class="apply-online clearfix">
                        <div class="left clearfix"> <span class="icon"><img src="{{asset('public/userend/images\apply-online-sm-ico.png')}}" class="img-responsive" alt=""></span> <span class="txt">Apply Online</span> </div>
                        <div class="arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    </a></div>
            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start Contact Us -->
    <section class="form-wrapper padding-lg">
        <div class="container">
            <form name="contact-form" id="ContactForm" action="{{ route('contact.store') }}" method="POST">
                @csrf
                <div class="row input-row">
                    <div class="col-sm-6">
                        <input name="first_name" type="text" placeholder="First Name" data-validation-required-message="This field is required">
                    </div>
                    <div class="col-sm-6">
                        <input name="last_name" type="text" placeholder="Last Name" data-validation-required-message="This field is required">
                    </div>
                </div>
                <div class="row input-row">
                    <div class="col-sm-6">
                        <input name="business_email" type="text" placeholder="Business Email" data-validation-required-message="This field is required">
                    </div>
                    <div class="col-sm-6">
                        <input name="phone_number" type="number" placeholder="Phone Number" data-validation-required-message="This field is required">
                    </div>
                </div>
                <div class="row input-row">
                    <div class="col-sm-6">
                        <input name="company" type="text" placeholder="Company">
                    </div>
                    <div class="col-sm-6">
                        <input name="job_title" type="text" placeholder="Job Tittle">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn">Apply Now <span class="icon-more-icon"></span></button>
                        <div class="msg"></div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- end Contact Us -->

    <!-- Start Google Map -->
    <section class="google-map">
        <div id="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.3360460381846!2d85.32086091415994!3d27.706908882792103!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb194500fda5fb%3A0x412b63f744b486d7!2sVito+Education+Pvt+Ltd.!5e0!3m2!1sen!2snp!4v1557914520134!5m2!1sen!2snp" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <div class="container">
            <div class="contact-detail">
                <div class="address">
                    <div class="inner">
                        <h3>{{ $site->name }}</h3>
                        <p>{{ $site->location }}</p>
                    </div>
                    <div class="inner">
                        <h3>{{ $site->phone }}</h3>
                    </div>
                    <div class="inner"> <a href="mailto:{{$site->email}}">{{$site->email}}</a> </div>
                </div>
                <div class="contact-bottom">
                    <ul class="follow-us clearfix">
                        <li><a href="{{$site->facebook}}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="{{ $site->twitter }}"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="{{ $site->youtube }}"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- End Google Map -->


@endsection

@section('scripts')
    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>

    <!-- Form validator JavaScript -->
    <script src="{{asset('public/adminpanel/js/pages/validation.js')}}"></script>
    <script>
        !function (window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        }(window, document, jQuery);
    </script>


    <script type="text/javascript">
        @if(session('flash_message'))
        swal("Success!", "{!! session('flash_message') !!}", "success")
        @endif
    </script>
@endsection