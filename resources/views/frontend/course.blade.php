@extends('frontend.layouts.design')

@section('content')


    <!-- Start Banner -->
    <div class="inner-banner blog"
         style="display: block; padding: 60px 0; min-height: 290px; background: url({{asset('public/adminpanel/uploads/pages/courses/'.$course->banner)}}) no-repeat center top / cover;">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-lg-9">
                    <div class="content">
                        <h1 style="color: white !important;">{{ $course->title }}</h1>
                        <p style="color: white !important;">{{ $course->sub_title }}</p>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-3"><a href="{{ route('apply') }}" class="apply-online clearfix">
                        <div class="left clearfix"><span class="icon"><img
                                        src="{{asset('public/userend/images\apply-online-sm-ico.png')}}"
                                        class="img-responsive" alt=""></span> <span class="txt">Apply Online</span>
                        </div>
                        <div class="arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    </a>
                    @if($course->brochure)
                        <a href="{{ asset('public/adminpanel/uploads/pages/brochure/'.$course->brochure) }}"
                           target="_blank" class="download-prospects brochure"> <span
                                    class="icon-brochure-icon"></span> <span class="small">Download:</span>Brochure</a>
                    @endif
                </div>

            </div>
        </div>
    </div>
    <!-- End Banner -->


    <!-- Start Course Description -->
    <section class="about inner padding-lg">
        
        <div class="container">

            <div class="row">
                <div class="col-md-7 col-md-push-5 left-block">
                    <h2>COURSE DESCRIPTION</h2>
                    {!! $course->description  !!}
                    {{--<div class="cert-head">--}}
                    {{--<h3>CERTIFICATION</h3>--}}
                    {{--</div>--}}
                    {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>--}}
                    {{--<ul class="cert-list clearfix">--}}
                    {{--<li>Lorem Ipsum is simply dummy text of the</li>--}}
                    {{--<li>Lorem Ipsum is simply dummy </li>--}}
                    {{--<li>Lorem Ipsum is simply dummy </li>--}}
                    {{--<li>Lorem Ipsum is simply dummy text of the</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="col-md-5 col-md-pull-7">
                    <div class="enquire-wrapper">
                        <figure class="hidden-xs hidden-sm"><img
                                    src="{{asset('public/adminpanel/uploads/pages/courses/'.$course->image)}}"
                                    class="img-responsive" alt=""></figure>
                        <div class="enquire-now">
                            <form action="{{ route('enquire.store') }}" method="POST">
                                @csrf
                                <div class="inner">
                                    <h3>Enquire now</h3>
                                    <div class="row1">
                                        <input name="name" type="text" placeholder="Name" required>
                                    </div>
                                    <div class="row2 clearfix">
                                        <input name="email" type="email" placeholder="Email" required>
                                        <input name="phone" type="text" placeholder="Phone" required>
                                    </div>
                                    <div class="row2 clearfix">
                                        <input name="place" type="text" placeholder="Place">
                                        <input name="course" type="text" placeholder="Course" required>
                                    </div>
                                </div>
                                <button class="enquire-btn">Enquire now <span class="icon-more-icon"></span></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="course-detail clearfix pull-right">
                        <a href="{{ route('apply') }}" class="btn">apply now <span class="icon-more-icon"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Course Description -->




@endsection