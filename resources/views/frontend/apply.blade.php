<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0; maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="images\favicon.ico">
    <title>Vito Education Consultancy</title>
    <!-- Reset CSS -->
    <link href="{{asset('public/userend/css/reset.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('public/userend/css/fonts.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('public/userend/assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{asset('public/userend/assets/select2/css/select2.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('public/userend/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Magnific Popup -->
    <link href="{{asset('public/userend/assets/magnific-popup/css/magnific-popup.css')}}" rel="stylesheet">
    <!-- Iconmoon -->
    <link href="{{asset('public/userend/assets/iconmoon/css/iconmoon.css')}}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link href="{{asset('public/userend/assets/owl-carousel/css/owl.carousel.min.css')}}" rel="stylesheet">
    <!-- Animate -->
    <link href="{{asset('public/userend/css/animate.css')}}" rel="stylesheet">
    <!-- Custom Style -->
    <link href="{{asset('public/userend/css/custom.css')}}" rel="stylesheet">
</head>
<body class="fill-bg">

<!-- Start Preloader -->
<div id="loading">
    <div class="element">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start apply online -->
<section class="login-wrapper register">
    <div class="inner">
        <div class="regiter-inner">
            <div class="login-logo"><a href=""><img src="images\login-logo.png" class="img-responsive" alt=""></a></div>
            <div class="head-block">
                <h1>Apply Now</h1>
            </div>
            <div class="cnt-block">
                <form action="{{ route('candidate.store') }}" method="POST" class="form-outer">
                    @csrf
                    <div class="row">
                        <div class="col-sm-6">
                            <input name="fname" type="text" placeholder="First Name" required>
                        </div>
                        <div class="col-sm-6">
                            <input name="lname" type="text" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <input name="address" type="text" placeholder="Address" required>
                        </div>
                        <div class="col-sm-6">
                            <select name="country" class="custom_select" required>
                                <option value="">-----Select Country------</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->name}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-sm-6">
                                <input name="email" type="email" placeholder="Email" required>
                            </div>
                        <div class="col-sm-6">
                            <input name="prefix" type="text" placeholder="+977" class="country-code" style=" width: 20%; ">
                            <input name="phone" type="number" placeholder="98xxxxxxxx" class="phone-no" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <input name="prefered_year" type="number" placeholder="Prefered Year" required>
                        </div>
                        <div class="col-sm-6">
                            <select name="course_id" class="custom_select" required>
                                <option value="">----Select Course-------</option>
                                @foreach($courses as $course)
                                    <option value="{{ $course->id }}">{{ $course->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <input name="qualification" type="text" placeholder="Education Qualification" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 clearfix">
                            <div class="col-left">
                                <h2>Gender</h2>
                                <ul class="select-opt clearfix">
                                    <li>
                                        <input id="f-option" name="gender" value="Male" type="radio" checked>
                                        <label for="f-option">Male</label>
                                        <div class="check"></div>
                                    </li>
                                    <li>
                                        <input id="s-option" name="gender" value="Female" type="radio">
                                        <label for="s-option">Female</label>
                                        <div class="check">
                                            <div class="inside"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-sm-6">

                        </div>
                    </div>
                    <div class="button-outer">
                        <button class="btn">Get Started Now <span class="icon-more-icon"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End apply online -->

<script src="{{asset('public/userend/js/jquery.min.js')}}"></script>
<!-- Bootsrap JS -->
<script src="{{asset('public/userend/assets/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Select2 JS -->
<script src="{{asset('public/userend/assets/select2/js/select2.min.js')}}"></script>
<!-- Match Height JS -->
<script src="{{asset('public/userend/assets/matchHeight/js/matchHeight-min.js')}}"></script>
<!-- Bxslider JS -->
<script src="{{asset('public/userend/assets/bxslider/js/bxslider.min.js')}}"></script>
<!-- Waypoints JS -->
<script src="{{asset('public/userend/assets/waypoints/js/waypoints.min.js')}}"></script>
<!-- Counter Up JS -->
<script src="{{asset('public/userend/assets/counterup/js/counterup.min.js')}}"></script>
<!-- Magnific Popup JS -->
<script src="{{asset('public/userend/assets/magnific-popup/js/magnific-popup.min.js')}}"></script>
<!-- Owl Carousal JS -->
<script src="{{asset('public/userend/assets/owl-carousel/js/owl.carousel.min.js')}}"></script>
<!-- Modernizr JS -->
<script src="{{asset('public/userend/js/modernizr.custom.js')}}"></script>
<!-- Custom JS -->
<script src="{{asset('public/userend/js/custom.js')}}"></script>

</body>
</html>