@section('style')
    .about.inner p {
    padding-bottom: 10px;
    color: black;
    }

@endsection
@include('frontend.layouts.head')

<!-- Start Banner -->
<div class="inner-banner blog"
     style="display: block; padding: 60px 0;  min-height: 290px;  background: url({{asset('public/adminpanel/uploads/pages/'.$message->banner)}}) no-repeat center top / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="content">
                    <h1 style="color: #fff;">{{$message->title}}</h1>
                    <p style="color: #fff;">{{$message->sub_title}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Banner -->


<!-- Start About -->
<section class="about inner padding-lg">
    <div class="container">
        <div class="row">
            <div class="col-md-7 left-block">
                {{--<h2>Who we are</h2>--}}
                {!! $message->content !!}
            </div>
            <div class="col-md-5 about-right"><img src="{{asset('public/adminpanel/uploads/pages/'.$message->image)}}"
                                                   class="img-responsive" alt=""></div>
        </div>
    </div>
</section>
<!-- End About -->


@include('frontend.layouts.footer')
