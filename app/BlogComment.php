<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    //
    protected $fillable = [
        'blog_post_id', 'full_name', 'comment', 'email'
    ];

    public function replies(){
        return $this->hasMany('App\BlogReply', 'blog_comment_id');
    }
}
