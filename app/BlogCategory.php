<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    //
    public function publish() {
        if ($this->status == 1) {
            return '<a href="' . route('category.publish', $this->id) . '"><span class="label label-success">Published</span> </a>';
        } else {
            return '<a href="' . route('category.publish', $this->id) . '"><span class="label label-danger">Unpublished</span> </a>';
        }
    }

    public function blogs() {
        return $this->hasMany('App\BlogPost', 'category_id', 'id');
    }
}
