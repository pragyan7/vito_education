<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineApply extends Model
{
    //
    public function Course() {
        return $this->belongsTo('App\Course');
    }

    public function seen() {
        if ($this->status == 1) {
            return '<a href="' . route('candidate.seen', $this->id) . '"><span class="label label-success">seen</span> </a>';
        } else {
            return '<a href="' . route('candidate.seen', $this->id) . '"><span class="label label-danger">unseen</span> </a>';
        }
    }
}
