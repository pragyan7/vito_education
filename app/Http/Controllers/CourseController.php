<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use File;
use Image;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $courses = Course::all();
        return view('admin.course.index')->with([
            'courses' => $courses,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $course = new Course();
        $data = $request->all();
        $course->title = $data['title'];
        $course->slug = str_slug($data['title']);
        $course->sub_title = $data['sub_title'];
        if(empty($data['description'])){
            $course->description = "";
        } else {
            $course->description = $data['description'];
        }


        if($request->hasFile('brochure')){
            $file = Input::file('brochure');
            if($file->isValid()){
                $extension = $file->getClientOriginalExtension();
                $filename = str_slug($data['title']).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/brochure/'.$filename;
                // Resize Image Code
                Image::make($file)->save($large_image_path);
                // Store image name in course table
                $course->brochure = $filename;
            }
        }

        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()/2).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/courses/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in course table
                $course->banner = $filename;
            }
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/courses/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(440, 290)->save($large_image_path);
                // Store image name in products table
                $course->image = $filename;
            }
        }

        $course->seo_title = $data['seo_title'];
        $course->seo_slug = $data['seo_slug'];
        $course->focus_keyphrase = $data['focus_keyphrase'];
        $course->meta_description = $data['meta_description'];
        $course->save();

        return redirect()->back()->with('flash_message', 'New Course added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $course = Course::find($id);
        return view('admin.course.edit')->with([
            'course' => $course,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
//        dd($request->all());
        $course = Course::find($id);
        $data = $request->all();
        $course->title = $data['title'];
        $course->slug = str_slug($data['title']);
        $course->sub_title = $data['sub_title'];
        if(empty($data['description'])){
            $course->description = "";
        } else {
            $course->description = $data['description'];
        }


        if($request->hasFile('brochure')){
            $file = Input::file('brochure');
            $brochure_path = 'adminpanel/uploads/pages/brochure/'.$course->brochure;
            if(File::exists($brochure_path)){
                File::delete($brochure_path);
            }
            if($file->isValid()){
                $extension = $file->getClientOriginalExtension();
                $filename = str_slug($data['title']).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/brochure/'.$filename;
                // Resize Image Code
                Image::make($file)->save($large_image_path);
                // Store image name in course table
                $course->brochure = $filename;
            }
        }

        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            $banner_path = 'adminpanel/uploads/pages/courses/'.$course->banner;
            if(File::exists($banner_path)){
                File::delete($banner_path);
            }
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()/2).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/courses/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in course table
                $course->banner = $filename;
            }
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            $image_path = 'adminpanel/uploads/pages/courses/'.$course->image;
            if(File::exists($image_path)){
                File::delete($image_path);
            }
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/courses/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(440, 290)->save($large_image_path);
                // Store image name in products table
                $course->image = $filename;
            }
        }

        $course->seo_title = $data['seo_title'];
        $course->seo_slug = $data['seo_slug'];
        $course->focus_keyphrase = $data['focus_keyphrase'];
        $course->meta_description = $data['meta_description'];
        $course->save();

        return redirect()->route('course.index')->with('flash_message', 'Course Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $course = Course::find($id);

        $banner_path = 'adminpanel/uploads/pages/courses/'.$course->banner;
        if(File::exists($banner_path)){
            File::delete($banner_path);
        }

        $image_path = 'adminpanel/uploads/pages/courses/'.$course->image;
        if(File::exists($image_path)){
            File::delete($image_path);
        }

        $brochure_path = 'adminpanel/uploads/pages/brochure/'.$course->brochure;
        if(File::exists($brochure_path)){
            File::delete($brochure_path);
        }

        $course->delete();

                
                return redirect()->back()->with('flash_message', 'Course Deleted Successfully');

    }

    public function brochuredestroy($id) 
    {
        $course = Course::find($id);

        $brochure_path = 'adminpanel/uploads/pages/brochure/'.$course->brochure;
        // dd($brochure_path);
        if(File::exists($brochure_path)){
            // dd($brochure_path);
            File::delete($brochure_path);
        }

        $course->brochure = null;
        $course->save();
        // dd($course);
        
        return redirect()->back()->with('flash_message', 'Brochure Deleted Successfully');

    }
}
