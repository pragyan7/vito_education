<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FaqPage;
use File;
use Illuminate\Support\Facades\Input;
use Image;

class FaqPageController extends Controller
{
    public function faqpage(){
      $faqpage = FaqPage::first();
      return view ('admin.page.faq', compact('faqpage'));
    }

    public function updatefaq(Request $request, $id){
//        dd($id);
      $faqpage = FaqPage::findOrFail($id);
      $data = $request->all();
      $faqpage->title = ucwords(strtolower($data['title']));
      $faqpage->sub_title = ucwords(strtolower($data['sub_title']));
      if($request->hasFile('banner')){
          $image_tmp = Input::file('banner');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(1000,9999).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/pages/faq/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
              // Store image name in products table
              $faqpage->banner = $filename;
          }
      }
      $faqpage->seo_title = $data['seo_title'];
      $faqpage->seo_slug = $data['seo_slug'];
      $faqpage->focus_keyphrase = $data['focus_keyphrase'];
      $faqpage->meta_description = $data['meta_description'];
      $faqpage->save();
      return redirect()->back()->with('flash_message', 'Faq page Updated Successfully');
    }
}
