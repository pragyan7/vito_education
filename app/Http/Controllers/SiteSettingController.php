<?php

namespace App\Http\Controllers;

use App\SiteSetting;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Input;
use Image;

class SiteSettingController extends Controller
{
    public function site(){
        $site = SiteSetting::first();
        return view ('admin.settings', compact('site'));
    }

    public function update(Request $request, $id){
        $site = SiteSetting::findOrFail($id);
        $data = $request->all();
        $site->name = ucwords(strtolower($data['name']));
        $site->email = strtolower($data['email']);
        $site->phone = $data['phone'];
        $site->facebook = $data['facebook'];
        $site->twitter = $data['twitter'];
        $site->youtube = $data['youtube'];
        $site->location = $data['location'];



        if($request->hasFile('header_logo')){
            $image_tmp = Input::file('header_logo');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(100,1000).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/site/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(200, 50)->save($large_image_path);
                // Store image name in products table
                $site->header_logo = $filename;
            }
        }

        if($request->hasFile('footer_logo')){
            $image_tmp = Input::file('footer_logo');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(1100,2200).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/site/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(356, 142)->save($large_image_path);
                // Store image name in products table
                $site->footer_logo = $filename;
            }
        }

        $site->save();
        return redirect()->back()->with('flash_message', 'Site Settings Has Been Successfully Updated');
    }
}
