<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Input;
use Image;

class TeamController extends Controller
{
    public function create(){
        return view ('admin.team.create');
    }

    public function store(Request $request){
        $data = $request->all();
        $team = new Team;
        $team->name = ucwords(strtolower($data['name']));
        $team->position = $data['position'];
        if(empty($data['description'])){
            $team->description = "";
        } else {
            $team->description = $data['description'];
        }
        $team->facebook = $data['facebook'];
        $team->linkedin = $data['linkedin'];
        $team->skype = $data['skype'];

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(100,1000).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/team/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(124,124)->save($large_image_path);
                // Store image name in products table
                $team->image = $filename;
            }
        }

        $team->save();
        return redirect()->route('team.index')->with('flash_message', 'Team Has Been Added Successfully');
    }

    public function index(){
        $teams = Team::latest()->get();
        return view ('admin.team.index', compact('teams'));
    }

    public function edit($id){
        $team = Team::findOrFail($id);
        return view ('admin.team.edit', compact('team'));
    }

    public function update(Request $request, $id){
        $team = Team::findOrFail($id);
        $data = $request->all();
        $team->name = ucwords(strtolower($data['name']));
        $team->position = $data['position'];
        if(empty($data['description'])){
            $team->description = "";
        } else {
            $team->description = $data['description'];
        }
        $team->facebook = $data['facebook'];
        $team->linkedin = $data['linkedin'];
        $team->skype = $data['skype'];

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(100,1000).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/team/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(124,124)->save($large_image_path);
                // Store image name in products table
                $team->image = $filename;
            }
        }

        $team->save();
        $image_path = $data['current_image'];

        if(!empty($data['image'])){
            if(File::exists($image_path)){
                File::delete($image_path);
            }
        }
        return redirect()->route('team.index')->with('flash_message', 'Team Has Been Updated Successfully');
    }



    public function delete($id){
        $team = Team::findOrFail($id);
        $image_path = $team->image;
        if(File::exists($image_path)){
            File::delete($image_path);
        }
        $team->delete();
        return redirect()->back()->with('flash_message', 'Team has been deleted Successfully');
    }
}
