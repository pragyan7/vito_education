<?php

namespace App\Http\Controllers;

use App\About;
use App\MessageMD;
use App\Prospectus;
use App\Service;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Input;
use Image;

class PagesController extends Controller
{
    public function about(){
        $about = About::first();
        return view ('admin.page.about', compact('about'));
    }

    public function updateabout(Request $request, $id){
        $about = About::findOrFail($id);
        $data = $request->all();
        $about->title = ucwords(strtolower($data['title']));
        $about->sub_title = ucwords(strtolower($data['sub_title']));
        if(empty($data['content'])){
            $about->content = "";
        } else {
            $about->content = $data['content'];
        }
        $about->youtube_link = $data['youtube_link'];
        $about->certified_courses = $data['certified_courses'];
        $about->students_serviced = $data['students_serviced'];
        $about->universities = $data['universities'];
        $about->students = $data['students'];

        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(1000,9999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in products table
                $about->banner = $filename;
            }
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(10000,99999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(1280, 853)->save($large_image_path);
                // Store image name in products table
                $about->image = $filename;
            }
        }

        $about->seo_title = $data['seo_title'];
        $about->seo_slug = $data['seo_slug'];
        $about->focus_keyphrase = $data['focus_keyphrase'];
        $about->meta_description = $data['meta_description'];

        $about->save();

        return redirect()->back()->with('flash_message', 'About page Updated Successfully');

    }

    public function service(){
        $service = Service::first();
        return view ('admin.page.service', compact('service'));
    }

    public function updateservice(Request $request, $id){
        $service = Service::findOrFail($id);
        $data = $request->all();
        $service->title = ucwords(strtolower($data['title']));
        $service->sub_title = ucwords(strtolower($data['sub_title']));
        if(empty($data['content'])){
            $service->content = "";
        } else {
            $service->content = $data['content'];
        }

        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in products table
                $service->banner = $filename;
            }
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(10000,99999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(458, 305)->save($large_image_path);
                // Store image name in products table
                $service->image = $filename;
            }
        }

        $service->seo_title = $data['seo_title'];
        $service->seo_slug = $data['seo_slug'];
        $service->focus_keyphrase = $data['focus_keyphrase'];
        $service->meta_description = $data['meta_description'];

        $service->save();

        return redirect()->back()->with('flash_message', 'Service page Updated Successfully');

    }



    public function prospectus(){
        $prospectus = Prospectus::first();
//        dd($prospectus);
        return view ('admin.page.prospectus', compact('prospectus'));
    }

    public function prospectusupdate(Request $request, $id){
        $validate = $this->validate($request, [
            'file' => 'nullable|mimes:pdf,docx,doc'
        ]);

//        if($validate->fails()) {
//            dd('failed');
//        }

        $prospectus = Prospectus::findOrFail($id);
        $data = $request->all();
        $prospectus->name = ucwords(strtolower($data['title']));
        $prospectus->slug = str_slug($data['title']);

        if($request->hasFile('file')){
            if($prospectus->file && app('files')->exists('public/adminpanel/uploads/prospectus/'.$prospectus->file)){
                app('files')->delete('public/adminpanel/uploads/prospectus/'.$prospectus->file);
            }
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension();
            $destination = 'public/adminpanel/uploads/prospectus/';
            $file_name = $data['title'].'.'.$ext;
            $file->move($destination, $file_name);
            $prospectus->file = $file_name;
        }

        $prospectus->save();

        return redirect()->back()->with('flash_message', 'Prospectus Updated Successfully');

    }

}
