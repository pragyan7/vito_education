<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partners;
use File;
use Illuminate\Support\Facades\Input;
use Image;

class PartnerController extends Controller
{
  public function create(){
      return view ('admin.partner.create');
  }

  public function store(Request $request){
      $data = $request->all();
      $partner = new Partners;
      $partner->title = ucwords(strtolower($data['title']));
      if($request->hasFile('image')){
          $image_tmp = Input::file('image');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(165,79).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/partner/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->resize(183, 94)->save($large_image_path);
              // Store image name in products table
              $partner->image = $filename;
          }
      }

      $partner->save();
      return redirect()->route('partner.index')->with('flash_message', 'Partner Has Been Added Successfully');
  }


  public function index(){
      $partners = Partners::latest()->get();
      return view ('admin.partner.index', compact('partners'));
  }

  public function edit($id){
      $partner = Partners::findOrFail($id);
      return view ('admin.partner.edit', compact('partner'));
  }

  public function update(Request $request, $id){
    $partner = Partners::findOrFail($id);
      $data = $request->all();
      $partner->title = ucwords(strtolower($data['title']));
      if($request->hasFile('image')){
          $image_tmp = Input::file('image');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(100,1000).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/partner/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->resize(100, 100)->save($large_image_path);
              // Store image name in products table
              $partner->image = $filename;
          }
      }

      $partner->save();
      return redirect()->route('partner.index')->with('flash_message', 'Partner Has Been Updated Successfully');
  }

  public function delete($id){
      $partner = Partners::findOrFail($id);
      $image_path = $partner->image;
      if(File::exists($image_path)){
          File::delete($image_path);
      }
      $partner->delete();
      return redirect()->back()->with('flash_message', 'Partner has been deleted Successfully');
  }
}
