<?php

namespace App\Http\Controllers;

use App\Enquire;
use App\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EnquireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $enquiries = Enquire::all();
        return view('admin.enquire', compact('enquiries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $enquire = new Enquire();

        $enquire->name = $request->input('name');
        $enquire->email = $request->input('email');
        $enquire->phone = $request->input('phone');
        $enquire->place = $request->input('place');
        $enquire->course = $request->input('course');

        $enquire->save();

        //mail send
        $site = SiteSetting::first()->email;
        $data = array('name' => $enquire->name,
                        'email' => $enquire->email,
                        'phone' => $enquire->phone,
                        'place' => $enquire->place,
                        'course' =>  $enquire->course,
        );

        Mail::send('admin.enquiry_mail', $data, function ($message) use($request, $site){
            $message->to($site, 'VITO education')->subject('Enquiry Mail');
            $message->from($request->input('email'), $request->input('name'));
        });



        return redirect()->back()->with('flash_message', 'Enquiry Placed, We will get back to you soon.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Enquire::find($id)->delete();

        return redirect()->back()->with('flash_message', 'One enquire deleted');
    }

    public function seen($id){
        $contact = Enquire::find($id);
        $contact->status = $contact->status? 0 : 1;
        $contact->save();
        return redirect()->back()->with('success', 'status changed');
    }
}
