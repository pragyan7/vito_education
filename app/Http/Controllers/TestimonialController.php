<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TestimonialPage;
use File;
use Illuminate\Support\Facades\Input;
use Image;
use App\Testimonial;

class TestimonialController extends Controller
{
  public function testimonialpage(){
    $page = TestimonialPage::first();
    return view ('admin.page.testimonial', compact('page'));

  }

  public function updatetestimonial(Request $request, $id){
    $page = TestimonialPage::findOrFail($id);
    $data = $request->all();
    $page->title = ucwords(strtolower($data['title']));
    $page->sub_title = ucwords(strtolower($data['sub_title']));
    if($request->hasFile('banner')){
        $image_tmp = Input::file('banner');
        if($image_tmp->isValid()){
            $extension = $image_tmp->getClientOriginalExtension();
            $filename = rand(1000,9999).'.'.$extension;
            $large_image_path = 'public/adminpanel/uploads/pages/testimonial/'.$filename;
            // Resize Image Code
            Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
            // Store image name in products table
            $page->banner = $filename;
        }
    }
    $page->seo_title = $data['seo_title'];
    $page->seo_slug = $data['seo_slug'];
    $page->focus_keyphrase = $data['focus_keyphrase'];
    $page->meta_description = $data['meta_description'];
    $page->save();
    return redirect()->back()->with('flash_message', 'Testimonial page Updated Successfully');
  }


  public function create(){
      return view ('admin.testimonial.create');
  }

  public function store(Request $request){
      $data = $request->all();
      $testimonial = new Testimonial;
      $testimonial->name = ucwords(strtolower($data['name']));
      $testimonial->designation = $data['designation'];
      if(empty($data['content'])){
          $testimonial->content = "";
      } else {
          $testimonial->content = $data['content'];
      }

      if($request->hasFile('image')){
          $image_tmp = Input::file('image');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(100,1000).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/testimonial/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->resize(100,100)->save($large_image_path);
              // Store image name in products table
              $testimonial->image = $filename;
          }
      }

      $testimonial->save();
      return redirect()->route('testimonial.index')->with('flash_message', 'Testimonial Has Been Added Successfully');
  }

  public function index(){
      $testimonials = Testimonial::latest()->get();
      return view ('admin.testimonial.index', compact('testimonials'));
  }

  public function edit($id){
      $testimonial = Testimonial::findOrFail($id);
      return view ('admin.testimonial.edit', compact('testimonial'));
  }

  public function update(Request $request, $id){
      $testimonial = Testimonial::findOrFail($id);
      $data = $request->all();
      $testimonial->name = ucwords(strtolower($data['name']));
      $testimonial->designation = $data['designation'];
      if(empty($data['content'])){
          $testimonial->content = "";
      } else {
          $testimonial->content = $data['content'];
      }
      if($request->hasFile('image')){
          $image_tmp = Input::file('image');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(100,1000).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/testimonial/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->resize(100,100)->save($large_image_path);
              // Store image name in products table
              $testimonial->image = $filename;
          }
      }
      $testimonial->save();
      $image_path = $data['current_image'];

      if(!empty($data['image'])){
          if(File::exists($image_path)){
              File::delete($image_path);
          }
      }
      return redirect()->route('testimonial.index')->with('flash_message', 'Testimonial Has Been Updated Successfully');
  }



  public function delete($id){
     
      $testimonial = Testimonial::findOrFail($id);
      $image_path = $testimonial->image;
      if(File::exists($image_path)){
          File::delete($image_path);
      }
      $testimonial->delete();
      return redirect()->back()->with('flash_message', 'Testimonial has been deleted Successfully');
  }
}
