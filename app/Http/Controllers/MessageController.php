<?php

namespace App\Http\Controllers;

use App\MessageMD;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MessageController extends Controller
{
    //
    public function message(){
        $message = MessageMD::first();
        return view ('admin.page.message', compact('message'));
    }

    public function updatemessage(Request $request, $id){
        $message = MessageMD::findOrFail($id);
        $data = $request->all();
        $message->title = $data['title'];
        $message->sub_title = $data['sub_title'];
        if(empty($data['content'])){
            $message->content = "";
        } else {
            $message->content = $data['content'];
        }

        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in products table
                $message->banner = $filename;
            }
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(10000,99999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(458, 305)->save($large_image_path);
                // Store image name in products table
                $message->image = $filename;
            }
        }

        $message->save();

        return redirect()->back()->with('flash_message', 'Message from MD page Updated Successfully');

    }
}
