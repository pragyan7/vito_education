<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = BlogCategory::all();
        return view('admin.blog.category', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.blog.create-category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = new BlogCategory();

        $category->title = $request->input('title');
        $category->slug = str_slug($request->input('title'));

        $category->save();

        return redirect()->back()->with('flash_message', 'Category added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = BlogCategory::find($id);
        return view('admin.blog.edit-category', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $category = BlogCategory::find($id);

        $category->title = $request->input('title');
        $category->slug = str_slug($request->input('title'));

        $category->save();

        return redirect()->back()->with('flash_message', 'Category Edited');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = BlogCategory::find($id);
//        if ($category->blog()->exists()) {
////            return redirect()->back()->withError('This category might have some product. Please confirmed and try again !');
//            $category->blog()->delete();
////            to delete all product of this category
//        }
        $category->delete();

        return redirect()->back()->with('flash_message', 'Category Deleted');

    }

    public function publish($id)
    {
        $category = BlogCategory::find($id);
        $category->status = $category->status ? 0 : 1;
        $category->save();
        return redirect()->back()->withSuccess('Status changed');
    }
}
