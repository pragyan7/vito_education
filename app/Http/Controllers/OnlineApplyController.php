<?php

namespace App\Http\Controllers;

use App\Course;
use App\OnlineApply;
use App\SiteSetting;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class OnlineApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $candidates = OnlineApply::orderBy('created_at', 'DESC')->get();
        return view('admin.online_applied', compact('candidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $candidate = new OnlineApply();
        $candidate->full_name = ucfirst($request->input('fname')).' '.ucfirst($request->input('lname'));
        $candidate->email = $request->input('email');
        $candidate->prefix = $request->input('prefix');
        $candidate->phone = $request->input('phone');
        $candidate->course_id = $request->input('course_id');
        $candidate->address = $request->input('address');
        $candidate->country = $request->input('country');
        $candidate->gender = $request->input('gender');
        $candidate->prefered_year = $request->input('prefered_year');
        $candidate->qualification = $request->input('qualification');

        $candidate->save();

        //mail send
        $course = Course::find($candidate->course_id)->title;
        $site = SiteSetting::first()->email;
        $data = array(
            'name' => $candidate->full_name,
            'email' => $candidate->email,
            'phone' => $candidate->prefix.' '.$candidate->phone,
            'course' => $course,
            'address' => $candidate->address,
            'country' => $candidate->country,
            'gender' => $candidate->gender,
            'prefered_year' => $candidate->prefered_year,
            'qualification' =>  $candidate->qualification,

        );

        Mail::send('admin.apply_mail', $data, function ($message) use($candidate, $site){
            $message->to($site, 'VITO education')->subject('Online Apply Mail');
            $message->from( $candidate->email, $candidate->full_name);
        });

        return redirect()->back()->with('flash_message', 'Form Submitted');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        OnlineApply::find($id)->delete();

        return redirect()->back()->with('flash_message', 'Candidate info deleted');
    }
    public function seen($id){
        $contact = OnlineApply::find($id);
        $contact->status = $contact->status? 0 : 1;
        $contact->save();
        return redirect()->back()->with('success', 'status changed');
    }
}
