<?php

namespace App\Http\Controllers;

use App\BlogCategory;
use App\BlogPost;
use App\Tag;
use Image;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogs = BlogPost::all();
        return view('admin.blog.blog', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tag::all();

        $categories = BlogCategory::all();
        return view('admin.blog.create-blog', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $blog = new BlogPost();

        $blog->title = $request->input('title');
        $blog->sub_title = $request->input('sub_title');
        $blog->slug = str_slug($request->input('title'));
        $blog->category_id = $request->input('category_id');
        $blog->summary = $request->input('summary');
        $blog->content = $request->input('content');
        if ($request->hasFile('image')) {
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()) . '.' . $extension;
                $large_image_path = 'public/adminpanel/uploads/blog/' . $filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(750, 415)->save($large_image_path);
                // Store image name in products table
                $blog->image = $filename;
            }
        }

        $blog->seo_title = $request->input('seo_title');
        $blog->seo_slug = $request->input('seo_slug');
        $blog->focus_keyphrase = $request->input('focus_keyphrase');
        $blog->meta_description = $request->input('meta_description');


        $blog->save();

        $blog->tags()->attach($request->input('tags'));

        return redirect()->back()->with('flash_message', 'New Blog added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tags = Tag::all();
        $blog = BlogPost::find($id);
        $categories = BlogCategory::all();
        return view('admin.blog.edit-blog', compact('blog', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $blog = BlogPost::find($id);

        $blog->title = $request->input('title');
        $blog->sub_title = $request->input('sub_title');
        $blog->slug = str_slug($request->input('title'));
        $blog->category_id = $request->input('category_id');
        $blog->summary = $request->input('summary');
        $blog->content = $request->input('content');
        if ($request->hasFile('image')) {
            $image_tmp = Input::file('image');
            $image_path = 'adminpanel/uploads/blog/'.$blog->image;
            if(File::exists($image_path)){
                File::delete($image_path);
            }
            if ($image_tmp->isValid()) {
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()) . '.' . $extension;
                $large_image_path = 'public/adminpanel/uploads/blog/' . $filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(750, 415)->save($large_image_path);
                // Store image name in products table
                $blog->image = $filename;
            }
        }

        $blog->seo_title = $request->input('seo_title');
        $blog->seo_slug = $request->input('seo_slug');
        $blog->focus_keyphrase = $request->input('focus_keyphrase');
        $blog->meta_description = $request->input('meta_description');


        $blog->save();

        $blog->tags()->sync($request->input('tags'));

        return redirect()->back()->with('flash_message', 'New Blog added Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $blog = BlogPost::find($id);

        $image_path = 'adminpanel/uploads/blog/'.$blog->image;
        if(File::exists($image_path)){
            File::delete($image_path);
        }

        if(count($blog->comments)>0){
            foreach ($blog->comments as $comment){
                $comment->replies()->delete();
            }
        }
        $blog->comments()->delete();
        $blog->tags()->detach();

        $blog->delete();

        return redirect()->back()->with('flash_message', 'Blog Deleted');
    }

    public function publish($id)
    {
        $blog = BlogPost::find($id);
        $blog->status = $blog->status ? 0 : 1;
        $blog->save();
        return redirect()->back()->withSuccess('Status changed');
    }
}
