<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use File;
use Illuminate\Support\Facades\Input;
use Image;


class SliderController extends Controller
{
  public function create(){
      return view ('admin.slider.create');
  }

  public function store(Request $request){
      $data = $request->all();
      $slider = new Slider;
      $slider->title = ucwords(strtolower($data['title']));
      $slider->subtitle = $data['subtitle'];
      if($request->hasFile('image')){
          $image_tmp = Input::file('image');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(100,1000).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/slider/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->resize(3000, 1000)->save($large_image_path);
              // Store image name in products table
              $slider->image = $filename;
          }
      }

      $slider->save();
      return redirect()->route('slider.index')->with('flash_message', 'Slider Has Been Added Successfully');
  }

  public function index(){
      $sliders = Slider::latest()->get();
      return view ('admin.slider.index', compact('sliders'));
  }


  public function edit($id){
      $slider = Slider::findOrFail($id);
      return view ('admin.slider.edit', compact('slider'));
  }

  public function update(Request $request, $id){
      $slider = Slider::findOrFail($id);
      $data = $request->all();
      $slider->title = ucwords(strtolower($data['title']));
      $slider->subtitle = $data['subtitle'];
      if($request->hasFile('image')){
          $image_tmp = Input::file('image');
          if($image_tmp->isValid()){
              $extension = $image_tmp->getClientOriginalExtension();
              $filename = rand(100,1000).'.'.$extension;
              $large_image_path = 'public/adminpanel/uploads/slider/'.$filename;
              // Resize Image Code
              Image::make($image_tmp)->save($large_image_path);
              // Store image name in products table
              $slider->image = $filename;
          }
      }

      $slider->save();
      $image_path = $data['current_image'];

      if(!empty($data['image'])){
          if(File::exists($image_path)){
              File::delete($image_path);
          }
      }
      return redirect()->route('slider.index')->with('flash_message', 'Slider Has Been Updated Successfully');
  }



  public function delete($id){
      $slider = Slider::findOrFail($id);
      $image_path = $slider->image;
      if(File::exists($image_path)){
          File::delete($image_path);
      }
      $slider->delete();
      return redirect()->back()->with('flash_message', 'Slider has been deleted Successfully');
  }
}
