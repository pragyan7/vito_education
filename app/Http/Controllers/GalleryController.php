<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryPage;
use File;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $galleries = Gallery::all();
        return view('admin.gallery.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallery = new Gallery();
        $data = $request->all();
        $gallery->title = ucwords(strtolower($data['title']));
        $gallery->sub_title = ucwords(strtolower($data['sub_title']));

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');

            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/gallery/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(400, 400)->save($large_image_path);
                // Store image name in products table
                $gallery->image = $filename;
            }
        }

        $gallery->save();

        return redirect()->back()->with('flash_message', 'Image Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $gallery = Gallery::find($id);
        return view('admin.gallery.edit', compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $gallery = Gallery::find($id);
        $data = $request->all();
        $gallery->title = ucwords(strtolower($data['title']));
        $gallery->sub_title = ucwords(strtolower($data['sub_title']));

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            $image_path = 'adminpanel/uploads/pages/gallery/'.$gallery->image;
            if(File::exists($image_path)){
                File::delete($image_path);
            }
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/gallery/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(400, 400)->save($large_image_path);
                // Store image name in products table
                $gallery->image = $filename;
            }
        }

        $gallery->save();

        return redirect()->back()->with('flash_message', 'Image Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $gallery = Gallery::find($id);
        $image_path = 'adminpanel/uploads/gallery/'.$gallery->image;
        if(File::exists($image_path)){
            File::delete($image_path);
        }
        $gallery->delete();

        return redirect()->back()->with('flash_message', 'Image Deleted Successfully');

    }

    public function gallerypage(){
        $gallery = GalleryPage::first();
        return view('admin.page.gallery', compact('gallery'));
    }

    public function updategallery(Request $request, $id){
        $gallery = GalleryPage::findOrFail($id);
        $data = $request->all();
        $gallery->title = ucwords(strtolower($data['title']));
        $gallery->sub_title = ucwords(strtolower($data['sub_title']));
        if($request->hasFile('banner')){
            $image_path = 'adminpanel/uploads/pages/gallery/'.$gallery->image;
            if(File::exists($image_path)){
                File::delete($image_path);
            }
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(1000,9999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/gallery/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in products table
                $gallery->banner = $filename;
            }
        }
        $gallery->seo_title = $data['seo_title'];
        $gallery->seo_slug = $data['seo_slug'];
        $gallery->focus_keyphrase = $data['focus_keyphrase'];
        $gallery->meta_description = $data['meta_description'];
        $gallery->save();
        return redirect()->back()->with('flash_message', 'Faq page Updated Successfully');
    }
}
