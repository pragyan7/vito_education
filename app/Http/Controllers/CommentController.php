<?php

namespace App\Http\Controllers;

use App\BlogComment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    //
    public function store(Request $request, $id)
    {
            BlogComment::create([
                'blog_post_id' => $id,
                'full_name' => $request->input('full_name'),
                'comment' => $request->input('comment'),
                'email' => $request->input('email')
            ]);

            return redirect()->back()->with('flash_message','Comment Added successfully..!');
    }
}
