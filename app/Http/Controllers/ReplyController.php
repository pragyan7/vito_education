<?php

namespace App\Http\Controllers;

use App\BlogReply;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    //
    public function store(Request $request, $id)
    {
        BlogReply::create([
            'blog_comment_id' => $id,
            'full_name' => $request->input('full_name'),
            'reply' => $request->input('reply'),
            'email' => $request->input('email')
        ]);

        return redirect()->back()->with('flash_message','Reply Added successfully..!');
    }
}
