<?php

namespace App\Http\Controllers;

use App\BlogPage;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BlogPageController extends Controller
{
    //
    public function blogpage(){
        $blog = BlogPage::first();
        return view ('admin.page.blog', compact('blog'));
    }

    public function updateblogpage(Request $request, $id){
        $blogpage = BlogPage::findOrFail($id);
        $data = $request->all();
        $blogpage->title = ucwords(strtolower($data['title']));
        $blogpage->sub_title = ucwords(strtolower($data['sub_title']));
        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(1000,9999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/blog/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in products table
                $blogpage->banner = $filename;
            }
        }
        $blogpage->seo_title = $data['seo_title'];
        $blogpage->seo_slug = $data['seo_slug'];
        $blogpage->focus_keyphrase = $data['focus_keyphrase'];
        $blogpage->meta_description = $data['meta_description'];
        $blogpage->save();
        return redirect()->back()->with('flash_message', 'Blog page Updated Successfully');
    }
}
