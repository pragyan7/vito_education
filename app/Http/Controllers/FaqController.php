<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;

class FaqController extends Controller
{
  public function create(){
      return view ('admin.faq.create');
  }

  public function store(Request $request){
      $data = $request->all();
      $faq = new Faq;
      $faq->question = ucwords(strtolower($data['question']));
      if(empty($data['answer'])){
        $faq->answer = "";
      } else {
        $faq->answer = $data['answer'];
      }
      $faq->save();
      return redirect()->route('faq.index')->with('flash_message', 'Faq Has Been Added Successfully');
  }

  public function index(){
      $faqs = Faq::latest()->get();
      return view ('admin.faq.index', compact('faqs'));
  }


  public function edit($id){
      $faq = Faq::findOrFail($id);
      return view ('admin.faq.edit', compact('faq'));
  }

  public function update(Request $request, $id){
      $faq = Faq::findOrFail($id);
      $data = $request->all();
      $faq->question = ucwords(strtolower($data['question']));
      if(empty($data['answer'])){
        $faq->answer = "";
      } else {
        $faq->answer = $data['answer'];
      }

      $faq->save();

      return redirect()->route('faq.index')->with('flash_message', 'Faq Has Been Updated Successfully');
  }



  public function delete($id){
      $faq = Faq::findOrFail($id);
      $faq->delete();
      return redirect()->back()->with('flash_message', 'Faq has been deleted Successfully');
  }
}
