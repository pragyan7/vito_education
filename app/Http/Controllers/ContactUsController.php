<?php

namespace App\Http\Controllers;

use App\Contact;
use App\ContactUsPage;
use App\SiteSetting;
use Illuminate\Support\Facades\Mail;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $inboxes = Contact::all();
        $info = SiteSetting::first();

        return view('admin.inbox', [
            'inboxes' => $inboxes,
            'info' => $info,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request->all());
        $contact = new Contact();

        $contact->fname = ucfirst($request->input('first_name'));
        $contact->lname = ucfirst($request->input('last_name'));
        $contact->company = $request->input('company');
        $contact->business_mail = $request->input('business_email');
        $contact->job_title = $request->input('job_title');
        $contact->phone_number = $request->input('phone_number');

        $contact->save();

        //mail send
        $site = SiteSetting::first()->email;
        $data = array('name' => $contact->fname.' '.$contact->lname,
                        'company' => $contact->company,
                        'phone' => $contact->phone_number,
                        'job_title' => $contact->job_title,
                        'email' => $contact->business_mail,
        );

        Mail::send('admin.contact_mail', $data, function ($message) use($request, $site){
           $message->to($site, 'VITO education')->subject('Contact Mail');
           $message->from($request->input('business_email'), $request->input('first_name').' '.$request->input('last name'));
        });

        return redirect()->back()->with('flash_message', 'Thank you for contacting us');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Contact::find($id)->delete();

        return redirect()->back()->with('flash_message', 'Deleted');
    }

    public function contactypage(){
        $contact = ContactUsPage::first();
        return view ('admin.page.contact', compact('contact'));
    }

    public function updatecontact(Request $request, $id) {
        $contact = ContactUsPage::findOrFail($id);
        $data = $request->all();
        $contact->title = ucwords(strtolower($data['title']));
        $contact->sub_title = ucwords(strtolower($data['sub_title']));
        if($request->hasFile('banner')){
            $image_tmp = Input::file('banner');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = rand(1000,9999).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/contact/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(2000, 590)->save($large_image_path);
                // Store image name in products table
                $contact->banner = $filename;
            }
        }
        $contact->seo_title = $data['seo_title'];
        $contact->seo_slug = $data['seo_slug'];
        $contact->focus_keyphrase = $data['focus_keyphrase'];
        $contact->meta_description = $data['meta_description'];
        $contact->save();
        return redirect()->back()->with('flash_message', 'Contact page Information Updated Successfully');
    }

    public function seen($id){
        $contact = Contact::find($id);
        $contact->status = $contact->status? 0 : 1;
        $contact->save();
        return redirect()->back()->with('success', 'ststus changed');
    }
}
