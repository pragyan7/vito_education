<?php

namespace App\Http\Controllers;

use App\PreparationClass;
use File;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PreparationClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $classes = PreparationClass::all();
        return view('admin.preparation.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.preparation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $class = new PreparationClass();
        $data = $request->all();
        $class->title = $data['title'];
        $class->slug = str_slug($data['title']);
        $class->sub_title = $data['sub_title'];
        if(empty($data['content'])){
            $class->content = "";
        } else {
            $class->content = $data['content'];
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/classes/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(100, 100)->save($large_image_path);
                // Store image name in products table
                $class->image = $filename;
            }
        }

        $class->seo_title = $data['seo_title'];
        $class->seo_slug = $data['seo_slug'];
        $class->focus_keyphrase = $data['focus_keyphrase'];
        $class->meta_description = $data['meta_description'];
        $class->save();

        return redirect()->back()->with('flash_message', 'New Preparation Class added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $class = PreparationClass::find($id);
        return view('admin.preparation.edit', compact('class'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $class = PreparationClass::find($id);

        $data = $request->all();
        $class->title = ucwords($data['title']);
        $class->slug = str_slug($data['title']);
        $class->sub_title = $data['sub_title'];
        if(empty($data['content'])){
            $class->content = "";
        } else {
            $class->content = $data['content'];
        }

        if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = md5(time()).'.'.$extension;
                $large_image_path = 'public/adminpanel/uploads/pages/classes/'.$filename;
                // Resize Image Code
                Image::make($image_tmp)->resize(100, 100)->save($large_image_path);
                // Store image name in products table
                $class->image = $filename;
            }
        }

        $class->seo_title = $data['seo_title'];
        $class->seo_slug = $data['seo_slug'];
        $class->focus_keyphrase = $data['focus_keyphrase'];
        $class->meta_description = $data['meta_description'];
        $class->save();

        return redirect()->route('preparation.index')->with('flash_message', 'Preparation Class Edited Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $class = PreparationClass::find($id);
        $image_path = 'adminpanel/uploads/pages/classes/'.$class->image;
        if(File::exists($image_path)){
            File::delete($image_path);
        }
        $class->delete();

    }
}
