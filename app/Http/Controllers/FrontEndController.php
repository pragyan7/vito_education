<?php

namespace App\Http\Controllers;

use App\About;
use App\BlogCategory;
use App\BlogPage;
use App\BlogPost;
use App\Branch;
use App\ContactUsPage;
use App\Country;
use App\Course;
use App\Gallery;
use App\GalleryPage;
use App\MessageMD;
use App\PreparationClass;
use App\Prospectus;
use App\Service;
use App\Tag;
use App\Team;
use App\Partners;
use Illuminate\Http\Request;
use App\Slider;
use App\FaqPage;
use App\Faq;
use App\TestimonialPage;
use App\Testimonial;
use App\SiteSetting;

class FrontEndController extends Controller
{
    public function front()
    {
        $branches = Branch::all();
        $prospectus = Prospectus::first();
        $site = SiteSetting::first();
        $partners = Partners::all();
        $testimonials = Testimonial::latest()->get();
        $classes = PreparationClass::all();
        $sliders = Slider::all();
        $about = About::first();
        $courses = Course::all();
        $blogs = BlogPost::orderBy('created_at', 'DESC')->limit(3)->get();
        $galleries = Gallery::orderBy('created_at', 'DESC')->limit(10)->get();;
        return view('frontend.index', compact('sliders',
            'about',
            'partners',
            'courses',
            'classes',
            'blogs',
            'galleries',
            'testimonials',
            'site',
            'prospectus',
            'branches'
        ));
    }

    public function about()
    {
        $branches = Branch::all();
        $about = About::first();
        $classes = PreparationClass::all();
        $teams = Team::all();
        $partners = Partners::latest()->get();
        return view('frontend.about', compact('about', 'teams', 'partners', 'classes', 'branches'));
    }

    public function service()
    {
        $service = Service::first();
        $about = About::first();
        $classes = PreparationClass::all();
        $teams = Team::all();
        $partners = Partners::latest()->get();
        return view('frontend.service', compact('about', 'teams', 'partners', 'classes', 'service'));
    }

    public function message()
    {
        $message = MessageMD::first();
        return view('frontend.message_md', compact('message'));
    }

    public function testimonial()
    {
        $page = TestimonialPage::first();
        $testimonials = Testimonial::latest()->get();
        return view('frontend.testimonial', compact('page', 'testimonials'));
    }

    public function faq()
    {
        $faqpage = FaqPage::first();
        $faqs = Faq::latest()->get();
        return view('frontend.faq', compact('faqpage', 'faqs'));
    }

    public function course($title)
    {
        $course = Course::where('slug', $title)->first();
        return view('frontend.course')->with([
            'course' => $course,
        ]);
    }

    public function gallery()
    {
        $page = GalleryPage::first();
        $galleries = Gallery::all();
        return view('frontend.gallery', compact('galleries', 'page'));
    }

    public function contact()
    {
        $page = ContactUsPage::first();
        return view('frontend.contact', compact('page'));
    }

    public function blog()
    {
        $blog = BlogPage::first();
        $blogs = BlogPost::where('status', 1)->orderBy('created_at', 'DESC')->paginate(12);
        return view('frontend.blog', compact('blogs', 'blog'));
    }

    public function blogsingle($id)
    {
        $comments = BlogPost::find($id)->comments;
        $categories = BlogCategory::where('status', 1)->get();
        $blogs = BlogPost::where('status', 1)->orderBy('created_at', 'ASC')->limit(5)->get();
        $blog = BlogPost::find($id);
        $tags = $blog->tags->toArray();
//        dd($tags->toArray());
        return view('frontend.blogsingle', compact('blog', 'categories', 'blogs', 'comments', 'tags'));
    }

    public function search(Request $request)
    {
        $key = $request->input('key');
        $type = $request->input('type');
        $results = null;
        if ($type == 'tag') {
            $tag = Tag::where('slug', $key)->first();
            $results = $tag->blogs()->paginate(12);
        }
        if ($type == 'search') {
            $results = BlogPost::where('title', 'LIKE', '%' . $key . '%')->paginate(12);
        }
        if ($type == 'category') {
            $category = BlogCategory::where('slug', $key)->first();
            $results = $category->blogs()->paginate(12);
        }

        return view('frontend.search', compact('results'));
    }

    public function terms()
    {
        return view('frontend.terms');
    }

    public function privacy()
    {
        return view('frontend.privacy');
    }

    public function apply()
    {
        $countries = Country::all();
        $courses = Course::all();
        return view('frontend.apply', compact('countries', 'courses'));
    }

    public function class($slug)
    {
        $class = PreparationClass::where('slug', $slug)->first();
        return view('frontend.class', compact('class'));
    }
}
