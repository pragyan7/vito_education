<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    public function blogs(){
        return $this->belongsToMany('App\BlogPost', 'blog_post_tag', 'tag_id', 'blog_id');
    }
}
