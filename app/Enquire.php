<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquire extends Model
{
    //
    public function seen() {
        if ($this->status == 1) {
            return '<a href="' . route('enquire.seen', $this->id) . '"><span class="label label-success">seen</span> </a>';
        } else {
            return '<a href="' . route('enquire.seen', $this->id) . '"><span class="label label-danger">unseen</span> </a>';
        }
    }
}
