<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    //
    public function publish() {
        if ($this->status == 1) {
            return '<a href="' . route('blog.publish', $this->id) . '"><span class="label label-success">Published</span> </a>';
        } else {
            return '<a href="' . route('blog.publish', $this->id) . '"><span class="label label-danger">Unpublished</span> </a>';
        }
    }

    public function category(){
        return $this->belongsTo('App\BlogCategory', 'category_id');
    }

    public function comments() {
        return $this->hasMany('App\BlogComment', 'blog_post_id', 'id');
    }

    public function tags() {
        return $this->belongsToMany('App\Tag', 'blog_post_tag', 'blog_id', 'tag_id');
    }
}
