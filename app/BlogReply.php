<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogReply extends Model
{
    //
    protected $fillable = [
        'blog_comment_id', 'full_name', 'reply', 'email'
    ];
}
