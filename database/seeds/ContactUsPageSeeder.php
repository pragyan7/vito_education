<?php

use Illuminate\Database\Seeder;

class ContactUsPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\ContactUsPage::insert([
            'banner' => "",
            'title' => "Contact Us",
            'sub_title' => "There is a soimething in every thing",
        ]);
    }
}
