<?php

use Illuminate\Database\Seeder;

class BlogPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\BlogPage::insert([
            'banner' => "",
            'title' => "Blog",
            'sub_title' => "There is a soimething in every thing",
        ]);
    }
}
