<?php

use Illuminate\Database\Seeder;

class GalleryPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        App\GalleryPage::insert([
            'banner' => "",
            'title' => "Gallery",
            'sub_title' => "There is a soimething in every thing",
        ]);
    }
}
